TEAM 1 - SHOPHUB README

FOLLOW THE INSTRUCTIONS IN THIS DOCUMENT TO RUN THE APPLICATION

-- REQUIREMENTS

To run this application, you will need to have installed:
IntelliJ IDE (with Gradle 7.2 and Java JDK 11)
MySQL Workbench
MySQL Server
Git Bash

-- STEP 1: CLONE THE PROJECT

Select a folder in your local directory to clone the project 
Launch Git Bash at the folder
Run command 'git clone https://git.cardiff.ac.uk/c2003457/shophub_team_1'

NOTE: Make yourself a brew. This could take some time.

You have now cloned the project. Launch the Gradle project in IntelliJ.


-- STEP 2: CONFIGURE THE DATABASE

This application uses a MySQL database (as well as a h2 database for testing).
You will need to create the database on your local MySQL server.

The database should be accessible at localhost:3306

The database schema can be found at /sqlScript/shophub_creation.sql
Run this script using MySQL Workbench to create the Shophub schema


-- STEP 3: CONFIGURE DATABASE ENCRYPTION

The database is encrypted and requires some configuration in IntelliJ

1. Click on edit configurations in then 'gradleProject [bootRun]' drop down menu at the top right of the screen
2. Select all existing configurations and delete them using the '-' button
3. Click edit configuration templates at the bottom left of the window
4. Locate 'Gradle' in the left-hand pane
5. In the environment variables field, input 'jasypt.encryptor.password=ShopHubDB' (this is a secret key. In production, this would not be stored in Git and would be held securely. However, for the purposes of assessing this work, this key has been made available)
6. Click apply


-- STEP 4: CONFIGURE BROWSER FOR HTTPS

This site uses HTTPS to secure data in transit. You may need to configure your browser to allow
insecure localhost (the data is still being secured but the padlock may not appear due to certification restrictions)

If you're using Chrome, type this into your browser and enable the option to allow insecure localhost sites
chrome://flags/#allow-insecure-localhost


-- STEP 4.1: TO START THE SERVER IN INTELLIJ

Use the 'bootRun' task in the Gradle menu to run the server.


-- STEP 4.2: TO START THE SERVER USING COMMAND LINE

Start a CMD window from the /jar folder at the repository route

Run command: 
java -Djasypt.encryptor.password=ShopHubDB -jar shophub-final.jar

If you are building the project from source, you will need to copy
the key file into the same directory as the jar. This can be found
at /gradleProject/springboot

You will also need to create an empty directory called temporaryMover in the same level as the jar

Alternatively, copy your newly built jar file into the /jar folder where both of these are present


-- STEP 5: USE THE APPLICATION

Go to https://localhost:8443/login

The default logins are:

    Email: william@eddy.com
    Password: 12345
    Role: Admin

    Email: jared@brokenshire.com
    Password: 12345
    Role: Retailer

    Email: Haroon@mohammed.com
    Password: 12345
    Role: Shopper


-- STEP 5: TO TEST

Use the 'test' task in the Gradle menu of INTELLIJ to run the automated tests
