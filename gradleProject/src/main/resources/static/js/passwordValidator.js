passwordInput.addEventListener("input", function (e) {
    const password = document.getElementById("passwordInput").value;
    const moreThan5Chars = document.getElementById("moreThan5Chars");
    const containsANumber = document.getElementById("containsANumber");

    if(password.length > 5){
        moreThan5Chars.style.color = "green";
    }
    else{
        moreThan5Chars.style.color = "lightGrey";
    }

    if(/\d/.test(password)){
        containsANumber.style.color = "green";
    }
    else{
        containsANumber.style.color = "lightGrey";
    }

});