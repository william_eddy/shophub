
$(function() {
    $("#discoverSearchBar").focusin(function() {
        $("#discoverOverlay").show();
        $("#discoverSearchBarResultsParent").show();
    });
});

function hideOverlay(){
$("#discoverOverlay").hide();
$("#discoverSearchBarResultsParent").hide();
}

$("#discoverOverlay").click(function(){
    hideOverlay();
});

discoverSearchBar.addEventListener("input", function (e) {
var search = document.getElementById("discoverSearchBar").value;
if (search.length > 0) {
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", '/api/search/all/' + search, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.onreadystatechange = function () {
    if (xhttp.readyState === 4) {
        if (xhttp.status === 200) {
            var searchResults = JSON.parse(xhttp.responseText);
            renderSearchResults(searchResults);
        } else {
            console.error(xhttp.statusText);
        }
    }else{
        clearSearchResults();
    }
};
    xhttp.send();
}
});

function renderSearchResults(searchResults) {
var oldSearchResultSection = document.getElementById("discoverSearchBarResultsParent");
var newSearchResultSection = document.createElement('div');
newSearchResultSection.setAttribute("id", "discoverSearchBarResultsParent");

var template = document.querySelector('#searchResultTemplate');
for (let resultKey in searchResults) {
var result = searchResults[resultKey];
result = Object.entries(result);
var clone = template.content.cloneNode(true);
var resultTextElement = clone.querySelectorAll("h3");
var resultIconElement = clone.querySelectorAll("i");
var resultLink = clone.querySelectorAll("a");

var context = result[0][1];

resultTextElement[0].innerHTML = result[1][1];

if (context == "tag"){
    resultIconElement[0].classList.add("fa-hashtag");
    resultLink[0].href = "/discover/"+ context +"/" + result[1][1];
}else if(context == "category"){
    resultIconElement[0].classList.add("fa-tag");
    resultLink[0].href = "/discover/"+ context +"/" + result[1][1];
}else{
    resultLink[0].href = "/discover/retailer/profile/" + result[1][1];
}

newSearchResultSection.appendChild(clone);
}
oldSearchResultSection.parentNode.replaceChild(newSearchResultSection, oldSearchResultSection);
document.getElementById("discoverSearchBar").blur();
document.getElementById("discoverSearchBar").focus();
}

function clearSearchResults(){
document.querySelector("#discoverSearchBarResultsParent").innerHTML = "";
}
