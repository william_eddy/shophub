selectedTags = [];

function selectTag(tagId){
    var bubble = document.getElementById("tagBubble" + tagId);
    console.log("tagBubble" + tagId);
    var text = document.getElementById("tagText" + tagId);
    if (selectedTags.includes(tagId)){
        const index = selectedTags.indexOf(tagId);
        selectedTags.splice(index);

        bubble.style.backgroundColor = "lightGrey";
        text.style.color = "white";
    }else{
        selectedTags.push(tagId);
        bubble.style.backgroundColor = "#5534F4";
        text.style.color = "white";
    }
    console.log(selectedTags);
}

$('#completeRegistrationButton').click(function() {
    event.preventDefault();

    if(selectedTags.length < 3){
        displayErrorMessage();
    }else {

        $.ajax({
            type: "POST",
            url: "/register/tags",
            data: JSON.stringify(selectedTags),
            contentType: "application/json; charset=utf-8",
            success: function (res) {
                if(res=="success"){
                    window.location.href = "/discover";
                }else{
                    displayErrorMessage();
                }
            },
            failure: function (errMsg) {
                console.log(errMsg);
            }
        });
    }
});


function displayErrorMessage(){
    $('html, body').animate({ scrollTop: 0 }, 'fast');
    document.getElementById("registerTagErrorParent").style.display = "block";
}