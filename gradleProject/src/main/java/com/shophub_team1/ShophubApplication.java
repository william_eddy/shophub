package com.shophub_team1;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.TypeExcludeFilter;
import org.springframework.context.annotation.ComponentScan;

import static org.springframework.context.annotation.FilterType.CUSTOM;

@EnableEncryptableProperties
@ComponentScan(basePackages = {"com.shophub_team1.jpa"},excludeFilters={@ComponentScan.Filter(type=CUSTOM,classes= TypeExcludeFilter.class),})
@SpringBootApplication
public class ShophubApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShophubApplication.class, args);
    }

}
