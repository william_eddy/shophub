package com.shophub_team1.jpa.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.List;

//entity for a social media platform
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name="social_media_platforms")
public class SocialMediaPlatformEntity {
    @Id
    private Long socialMediaPlatformId;
    private String name;
    private String iconClassName;
    private String hexCode;

    @OneToMany(mappedBy="platform",cascade= CascadeType.ALL, fetch = FetchType.LAZY)
    private List<RetailerSocialMediaEntity> platforms;
}
