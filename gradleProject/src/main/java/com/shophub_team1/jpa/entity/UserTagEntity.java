package com.shophub_team1.jpa.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

//entity for join table user_tags
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name="user_tags")
public class UserTagEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long userTagId;
    private Long userId;
    @Column(name="tag_id")
    private Long tagId;

    public UserTagEntity(Long userId, Long tagId){
        this.userId = userId;
        this.tagId = tagId;
    }
}
