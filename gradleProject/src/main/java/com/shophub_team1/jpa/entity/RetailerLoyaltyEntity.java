package com.shophub_team1.jpa.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

//entity for join table retailer_loyalty
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name="retailer_loyalty")
public class RetailerLoyaltyEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long retailerLoyaltyId;
    private Long userId;
    private Integer points;
    private Integer pointsRedeemed;

    @JsonIgnore
    @ManyToOne()

    @JoinColumn(name="retailer_id")
    private RetailerEntity retailer;

    public RetailerLoyaltyEntity(Long userId, Long id, Integer points, Integer pointsRedeemed) {
        this.userId = userId;
        this.retailer = new RetailerEntity(id);
        this.points = points;
        this.pointsRedeemed = pointsRedeemed;
    }


}

