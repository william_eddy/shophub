package com.shophub_team1.jpa.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

//entity for a log
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name="logging")
public class LoggingEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long loggingId;
    private Long eventId;
    private String details;
    private String username;
    private java.sql.Timestamp timestamp;

    public LoggingEntity(Long eventId, String details, String username){
        this.eventId = eventId;
        this.details = details;
        this.username = username;
    }


}
