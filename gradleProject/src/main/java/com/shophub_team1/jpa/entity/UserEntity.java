package com.shophub_team1.jpa.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

//entity for a user
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name="users")
public class UserEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long userId;
    private String firstName;
    private String lastName;
    private String emailAddress;
    private String password;
    private String profilePicture;
    @ManyToOne()
    @JoinColumn(name="role_id", insertable = false, updatable = false)
    private RoleEntity role;

    public UserEntity(String firstName, String lastName, String emailAddress, String password, String profilePicture){
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddress = emailAddress;
        this.password = password;
        this.profilePicture = profilePicture;
    }

}
