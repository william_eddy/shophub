package com.shophub_team1.jpa.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.List;
import java.util.Objects;

//entity for a category
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name="categories")
public class CategoryEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long categoryId;
    private String name;

    @JsonIgnore
    @OneToMany(mappedBy="category",cascade=CascadeType.ALL, fetch = FetchType.LAZY)
    private List<RetailerEntity> retailers;

    public CategoryEntity(Long categoryId){
        this.categoryId = categoryId;
    }

    public CategoryEntity(Long categoryId, String name){
        this.categoryId = categoryId;
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CategoryEntity categoryEntity = (CategoryEntity) o;
        return categoryId == categoryEntity.categoryId && name.equals(categoryEntity.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(categoryId, name, retailers);
    }
}
