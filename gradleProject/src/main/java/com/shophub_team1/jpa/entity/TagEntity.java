package com.shophub_team1.jpa.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

//entity for a tag
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name="tags")
public class TagEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="tag_id")
    private Long tagId;
    private String name;

    @JsonIgnore
    @ManyToMany(mappedBy = "tags")
    private List<RetailerEntity> posts = new ArrayList<>();
}
