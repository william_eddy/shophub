package com.shophub_team1.jpa.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

//entity for a retailer
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Transactional
@Table(name="retailers")
public class RetailerEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long retailerId;
    private String name;
    private String description;
    private String logo;
    private String backgroundImage;
    private Integer maxPoints;
    private Long userId;
    private String websiteUrl;

    @ManyToOne()
    @JoinColumn(name="category_id")
    private CategoryEntity category;

    @OneToMany(mappedBy="retailer",cascade=CascadeType.ALL, fetch = FetchType.LAZY)
    private List<RetailerLoyaltyEntity> retailerLoyalties;

    @OneToMany(mappedBy="retailer",cascade=CascadeType.ALL, fetch = FetchType.LAZY)
    private List<RetailerSocialMediaEntity> socialMedias;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.ALL})
    @JoinTable(name = "retailer_tags", joinColumns = @JoinColumn(name = "retailer_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id"))
    private List<TagEntity> tags = new ArrayList<>();


    public RetailerEntity(String name, String description, String logo, String backgroundImage, Integer maxPoints, Long userId, Long categoryId, String websiteUrl){
        this.name = name;
        this.description = description;
        this.logo = logo;
        this.backgroundImage = backgroundImage;
        this.maxPoints = maxPoints;
        this.userId = userId;
        this.category = new CategoryEntity(categoryId);
        this.websiteUrl = websiteUrl;
    };

    public RetailerEntity(Long id) {
        this.retailerId = id;
    }

    public RetailerEntity(Long retailerId, String name, String description, String logo, String backgroundImage, Integer maxPoints, Long userId, Long categoryId, String websiteUrl){
        this.retailerId = retailerId;
        this.name = name;
        this.description = description;
        this.logo = logo;
        this.backgroundImage = backgroundImage;
        this.maxPoints = maxPoints;
        this.userId = userId;
        this.category = new CategoryEntity(categoryId);
        this.websiteUrl = websiteUrl;
    };

    public RetailerEntity(String name, String description, String logo, String backgroundImage, Integer maxPoints, Long userId, CategoryEntity category, String websiteUrl){
        this.name = name;
        this.description = description;
        this.logo = logo;
        this.backgroundImage = backgroundImage;
        this.maxPoints = maxPoints;
        this.userId = userId;
        this.category = category;
        this.websiteUrl = websiteUrl;
    };

    public RetailerEntity(Long retailerId, String name, String description, String logo, String backgroundImage, Integer maxPoints, Long userId, CategoryEntity category, String websiteUrl){
        this.retailerId = retailerId;
        this.name = name;
        this.description = description;
        this.logo = logo;
        this.backgroundImage = backgroundImage;
        this.maxPoints = maxPoints;
        this.userId = userId;
        this.category = category;
        this.websiteUrl = websiteUrl;
    };

    //used for testing purposes
    public RetailerEntity(String name, Long userId, CategoryEntity category){
        this.name = name;
        this.userId = userId;
        this.category = category;
    }


}
