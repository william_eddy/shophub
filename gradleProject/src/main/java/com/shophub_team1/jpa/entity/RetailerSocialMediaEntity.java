package com.shophub_team1.jpa.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

//entity for join table retailer_social_media
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name="retailer_social_media")
public class RetailerSocialMediaEntity {
    @Id
    private String retailerSocialMediaId;
    @Column(name="retailer_id")
    private String retailerId;
    private String socialMediaUrl;

    @JsonIgnore
    @ManyToOne()
    @JoinColumn(name="retailer_id", insertable = false, updatable = false)
    private RetailerEntity retailer;

    @JsonIgnore
    @ManyToOne()
    @JoinColumn(name="social_media_platform_id", insertable = false, updatable = false)
    private SocialMediaPlatformEntity platform;




}
