package com.shophub_team1.jpa.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

//entity for an event
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name="event")
public class EventEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long eventId;
    private String name;


}
