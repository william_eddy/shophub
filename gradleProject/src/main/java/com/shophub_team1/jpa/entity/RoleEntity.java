package com.shophub_team1.jpa.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

//entity for a user role
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name="roles")
public class RoleEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long roleId;

    private String name;

    @JsonIgnore
    @OneToMany(targetEntity=UserEntity.class, mappedBy="role",cascade=CascadeType.ALL, fetch = FetchType.EAGER)
    private List<UserEntity> user = new ArrayList<>();

    public RoleEntity(Long roleId){
        this.roleId = roleId;
    }

    public RoleEntity(Long roleId, String name){
        this.roleId = roleId;
        this.name = name;
    }
}
