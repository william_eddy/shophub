package com.shophub_team1.jpa.security;

import com.shophub_team1.jpa.service.LoggingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

//custom handler for failed login attempts
public class CustomAuthenticationFailureHandler implements AuthenticationFailureHandler {

    @Autowired
    private LoggingService loggingService;

    //log failed login attempt and redirect to login page with error message
    @Override
    public void onAuthenticationFailure(
            HttpServletRequest request,
            HttpServletResponse response,
            AuthenticationException exception)
            throws IOException, ServletException {

        Map<String, String> errors = new HashMap<String, String>();
        errors.put("error", "Invalid username or password");

        loggingService.saveLog(3L, exception.getMessage());
        request.setAttribute("param", errors);
        response.sendRedirect("/login?error");
    }
}
