package com.shophub_team1.jpa.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.header.HeaderWriterFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

//spring security login configuration
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    UserDetailsService userDetailsService;

    //login configuration
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().ignoringAntMatchers("/register/accountDetails")
                .and()
                .csrf().ignoringAntMatchers("/register/tags")
                .and()
                .addFilterBefore(new CSPNonceFilter(), HeaderWriterFilter.class)
                .authorizeRequests()
                .mvcMatchers("/discover/**").access("hasAuthority('Admin') or hasAuthority('Shopper') or hasAuthority('Retailer')")
                .mvcMatchers("/retailer/**").access("hasAuthority('Admin') or hasAuthority('Shopper') or hasAuthority('Retailer')")
                .mvcMatchers("/profile/**").access("hasAuthority('Admin') or hasAuthority('Shopper') or hasAuthority('Retailer')")
                .mvcMatchers("/").access("hasAuthority('Admin') or hasAuthority('Shopper') or hasAuthority('Retailer')")
                .mvcMatchers("").access("hasAuthority('Admin') or hasAuthority('Shopper') or hasAuthority('Retailer')")
                .mvcMatchers("/api/**").permitAll()
                .mvcMatchers("/test").permitAll()
                .mvcMatchers("/admin/**").access("hasAuthority('Admin')")
                .mvcMatchers("/css/**").permitAll()
                .mvcMatchers("/js/**").permitAll()
                .mvcMatchers("/images/**").permitAll()
                .mvcMatchers("/login").permitAll()
                .mvcMatchers("/login**").permitAll()
                .mvcMatchers("/register/accountDetails").permitAll()
                .mvcMatchers("/register/tags").permitAll()
                .mvcMatchers("/api/currentUser").permitAll()
                .antMatchers("/favicon.ico").permitAll()
                .anyRequest().denyAll()
                .and()
                .formLogin()
                    .successHandler(authenticationSuccessHandler())
                    .failureHandler(authenticationFailureHandler())
                    .loginPage("/login")
                    .permitAll()
                .and()
                .logout()
                    .logoutRequestMatcher(new AntPathRequestMatcher("/logout", "GET"))
                    .logoutSuccessUrl("/login")
                .and()
                .headers()
                .xssProtection()
                .and()
                .contentSecurityPolicy("" +
                        "default-src 'self' 'nonce-{nonce}' " +
                            "https://ajax.googleapis.com/ajax/ " +
                            "https://ssl.google-analytics.com/ " +
                            "https://www.pagespeed-mod.com/v1/ " +
                            "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/ " +
                            "https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/" +
                        ";style-src 'self' 'nonce-{nonce}' " +
                            "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/ " +
                            "https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/"

                );
    }

    //wire custom login failure handler
    @Bean
    public AuthenticationFailureHandler authenticationFailureHandler() {
        return new CustomAuthenticationFailureHandler();
    }

    //wire custom login success handler
    @Bean
    public AuthenticationSuccessHandler authenticationSuccessHandler() {
        return new CustomAuthenticationSuccessHandler();
    }

    //wire user details service
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    //default password encoder
    @Bean
    public PasswordEncoder getPasswordEncoder(){
        return NoOpPasswordEncoder.getInstance();
    }

    //permit all to resources directory
    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers("/resources/**");
    }

}

