package com.shophub_team1.jpa.security;

import com.shophub_team1.jpa.entity.UserEntity;
import com.shophub_team1.jpa.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import java.util.Optional;

//user details service for login service
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    //adapted from JavaBrains
    //accessed at 11:50 02/12/2021
    //https://www.youtube.com/watch?v=TNt3GHuayXs

    //get the user data for the given username
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{
        Optional<UserEntity> user = userRepository.findByEmailAddress(username);
        user.orElseThrow(() -> new UsernameNotFoundException("Username not found: " + username));

        return user.map(UserDetailsImpl::new).get();
    }

    //end of reference
}
