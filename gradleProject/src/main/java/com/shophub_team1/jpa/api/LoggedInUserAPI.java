
package com.shophub_team1.jpa.api;
import com.shophub_team1.jpa.miscObjects.UserNavbar;
import com.shophub_team1.jpa.service.LoggedInUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//A controller for the database test route
@RestController
@RequestMapping("api/userSession/")
public class LoggedInUserAPI {

    @Autowired
    private LoggedInUserService loggedInUserService;

    //Get the first name of the logged in user
    @GetMapping("/firstName")
    public String getFirstName() {
        return loggedInUserService.getFirstName();
    }

    @GetMapping("/navbar")
    public UserNavbar getNavbarInfo() {
        UserNavbar userNavbar = new UserNavbar(loggedInUserService.getFirstName(), loggedInUserService.getProfilePicture());
        return userNavbar;
    }

    @GetMapping("/lastName")
    public String getlastName() {
        return loggedInUserService.getLastName();
    }
}
