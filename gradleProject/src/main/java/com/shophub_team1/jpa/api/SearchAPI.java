package com.shophub_team1.jpa.api;

import com.shophub_team1.jpa.miscObjects.SearchResult;
import com.shophub_team1.jpa.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

//A controller for the database test route
@RestController
@RequestMapping("api/search/")
public class SearchAPI {

    @Autowired
    private SearchService searchService;

    //Return a search mix of retailers, categories and tags
    @GetMapping("all/{search}")
    public List<SearchResult> getAllSearchResults(@PathVariable(value = "search", required = true) String search) {
        return searchService.getAllSearchResults(search);
    }

}
