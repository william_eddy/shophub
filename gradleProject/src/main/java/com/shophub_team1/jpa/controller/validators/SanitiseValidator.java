package com.shophub_team1.jpa.controller.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SanitiseValidator implements ConstraintValidator<SanitiseConstraint, String> {

    @Override
    public boolean isValid(String astring, ConstraintValidatorContext constraintValidatorContext) {
        if (astring.matches("^[a-zA-z0-9@'. ]+$")) {
            return true;
        }
        return false;
    }
}
