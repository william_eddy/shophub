package com.shophub_team1.jpa.controller.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = NewEmailValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface NewEmailConstraint {
    String message() default "This email address has already been registered";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
