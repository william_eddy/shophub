package com.shophub_team1.jpa.controller;

import com.shophub_team1.jpa.controller.forms.AdminAddRetailersForm;
import com.shophub_team1.jpa.controller.forms.UpdateRetailersForm;
import com.shophub_team1.jpa.entity.CategoryEntity;
import com.shophub_team1.jpa.entity.RetailerEntity;
import com.shophub_team1.jpa.miscObjects.LoggedInUserAttribute;
import com.shophub_team1.jpa.service.*;
import com.shophub_team1.jpa.service.AdminAddRetailerService;
import com.shophub_team1.jpa.service.ImageService;
import com.shophub_team1.jpa.service.LoggingService;
import com.shophub_team1.jpa.service.RetailerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

//administration controller
@Controller
@RequestMapping("/admin")
public class AdminController extends LoggedInUserAttribute {

    @Autowired
    private AdminAddRetailerService adminAddRetailerService;

    @Autowired
    private LoggingService loggingService;

    @Autowired
    private ImageService imageService;

    @Autowired
    private RetailerService retailerService;

    @Autowired
    private UpdateRetailerService updateRetailerService;


    //renders template for admin home
    @GetMapping({"/home"})
    public String adminHome(Model model){
        return "adminHome.html";
    }

    //renders the new retailers form
    @GetMapping("/retailer/new")
    public String newRetailer(Model model) {
        AdminAddRetailersForm retailerForm = new AdminAddRetailersForm();
        model.addAttribute("adminAddRetailersForm", retailerForm);
        return "add-retailers.html";
    }

    // Thymeleaf returns post mapping as an action, triggering this event.
    // It takes any errors discovered with the form validation via the bindingResult given by thymeleaf
    // and outputs them by re-rendering the page. However, in the case of a success, the data is saved.
    @PostMapping("/retailer/new")
    public String retailerFormPost(@Valid AdminAddRetailersForm adminAddRetailersForm,BindingResult bindingResult, Model model) throws IOException {
        MultipartFile backgroundImage = adminAddRetailersForm.getBackgroundImage();
        MultipartFile logo = adminAddRetailersForm.getLogo();
        String categoryName = adminAddRetailersForm.getCategoryName();
        Optional<Long> categoryID = adminAddRetailerService.getCategoryIdByName(categoryName);

        if (bindingResult.hasErrors()) {
            System.out.println("Errors: " + bindingResult.getAllErrors());
            return "add-retailers.html";
        }else{
            imageService.saveImage(logo, "retailers/logos/");
            imageService.saveImage(backgroundImage, "retailers/backgrounds/");

            RetailerEntity newRetailer = new RetailerEntity(adminAddRetailersForm.getName(),
                    adminAddRetailersForm.getDescription(),
                    logo.getOriginalFilename().toLowerCase(),
                    backgroundImage.getOriginalFilename().toLowerCase(),
                    adminAddRetailersForm.getMaxPoints(),
                    adminAddRetailersForm.getUserId(),
                    new CategoryEntity(categoryID.get(), adminAddRetailersForm.getCategoryName()),
                    adminAddRetailersForm.getWebsiteUrl());

            retailerService.save(newRetailer);
            loggingService.saveLog(4L,"Retailer added: " + adminAddRetailersForm.getName());
        }

        return "redirect:/admin/success";
    }

    //A route to render a user profile template
    @GetMapping("/retailer/edit/{retailerName}")
    public String showRetailerUpdate(
            @PathVariable(value = "retailerName") String retailerName,
            Model model) {

        retailerName = URLDecoder.decode(retailerName, StandardCharsets.UTF_8);
        RetailerEntity givenRetailer = retailerService.getRetailerByName(retailerName);

        UpdateRetailersForm form = new UpdateRetailersForm();
        form.updateRetailersFormPreLoad(givenRetailer);
        model.addAttribute("updateRetailersForm", form);

        return "edit-retailer.html";
    }

    //renders the operation success template
    @GetMapping("/success")
    public String formSuccess(Model model){
        return "formSuccess.html";
    }

    //updates the retailer from the form data
    @PostMapping("/retailer/edit")
    public String retailerEditFormPost(@Valid UpdateRetailersForm updateRetailersForm, BindingResult bindingResult, Model model) throws IOException, InterruptedException {
        System.out.println("Retailer edit posted");

        if (bindingResult.hasErrors()) {
            System.out.println("Errors");
            System.out.println("Errors: " + bindingResult.getAllErrors());
            return "edit-retailer.html";
        }

        updateRetailerService.formToDatabase(updateRetailersForm);
        loggingService.saveLog(5L, "Retailer changed: " + updateRetailersForm.getName());
        return "redirect:/admin/success";
    }

}


