package com.shophub_team1.jpa.controller.validators;

import com.shophub_team1.jpa.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NewEmailValidator implements
        ConstraintValidator<NewEmailConstraint, String> {

    @Autowired
    UserService userService;

    @Override
    public void initialize(NewEmailConstraint emailAddress) {
    }

    @Override
    public boolean isValid(String field,
                           ConstraintValidatorContext cxt) {
        return userService.isEmailAddressNew(field);
    }

}
