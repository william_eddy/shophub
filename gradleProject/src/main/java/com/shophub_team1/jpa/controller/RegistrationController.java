package com.shophub_team1.jpa.controller;

import com.shophub_team1.jpa.controller.forms.AddNewUserForm;
import com.shophub_team1.jpa.entity.TagEntity;
import com.shophub_team1.jpa.service.LoggedInUserService;
import com.shophub_team1.jpa.miscObjects.LoggedInUserAttribute;
import com.shophub_team1.jpa.service.TagService;
import com.shophub_team1.jpa.service.UserService;
import com.shophub_team1.jpa.service.UserTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

//registration controller
@Controller
@RequestMapping("/register")
public class RegistrationController extends LoggedInUserAttribute {

    @Autowired
    UserService userService;

    @Autowired
    TagService tagService;

    @Autowired
    UserTagService userTagService;

    @Autowired
    LoggedInUserService loggedInUserService;

    //renders the account details section of the registration process
    @GetMapping({"/accountDetails"})
    public String accountDetailsGet(Model model){
        AddNewUserForm addNewUserForm = new AddNewUserForm();
        model.addAttribute("addNewUserForm", addNewUserForm);
        return "registerAccountDetails.html";
    }

    //saves the registration account details in the database and creates a new user
    @PostMapping("/accountDetails")
    public String accountDetailsPost(@Valid AddNewUserForm addNewUserForm, BindingResult bindingResult, Model model, HttpServletRequest req, String user, String pass) throws IOException {

        String firstName = addNewUserForm.getFirstName();
        String lastName = addNewUserForm.getLastName();
        String emailAddress = addNewUserForm.getEmailAddress();
        String password = addNewUserForm.getPassword();


        if (bindingResult.hasErrors()) {
            System.out.println("Errors: " + bindingResult.getAllErrors());
            return "registerAccountDetails.html";
        }else{
            userService.save(firstName,lastName,emailAddress,password,"defaultProfile.jpg");
        }

        Authentication auth =
                new UsernamePasswordAuthenticationToken(emailAddress, password, Arrays.asList(new SimpleGrantedAuthority("Shopper")));

        SecurityContextHolder.getContext().setAuthentication(auth);

        return "redirect:/register/tags";
    }

    //renders the tag selection part of the registration process
    @GetMapping({"/tags"})
    public String tags(Model model){
        List<TagEntity> tags = tagService.getTags();
        model.addAttribute("tags",tags);
        return "registerTags.html";
    }

    //saves the tags the user has selected in the registration process
    @PostMapping({"/tags"})
    public @ResponseBody String confirm(@RequestBody Long[] selectedTags) {
        if(selectedTags.length < 3){
            return "more_tags_required";
        }
        int i;
        for (i = 0; i < selectedTags.length; i++) {
            userTagService.save(loggedInUserService.getUserId(),selectedTags[i]);
        }
        return "success";
    }
}
