package com.shophub_team1.jpa.controller.validators;

import com.shophub_team1.jpa.service.UserService;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class RetailerAddUserIDValidation implements ConstraintValidator<RetailerAddUserID, Long> {

    private final UserService userService;

    public RetailerAddUserIDValidation(UserService service) {
        userService = service;
    }

    @Override
    public boolean isValid(Long UserId, ConstraintValidatorContext constraintValidatorContext) {
        return userService.doesIdExist(UserId);
    }
}
