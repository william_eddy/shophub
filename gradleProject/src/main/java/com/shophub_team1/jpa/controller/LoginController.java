package com.shophub_team1.jpa.controller;

import com.shophub_team1.jpa.miscObjects.LoggedInUserAttribute;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

//login controller
@Controller
public class LoginController extends LoggedInUserAttribute {

    //renders the login form
    @GetMapping({"/","/login"})
    public String loginHome(){
        return "login.html";
    }

}
