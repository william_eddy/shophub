package com.shophub_team1.jpa.controller.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ContainsNumberValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ContainsNumberConstraint {
    String message() default "Must contain a number";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
