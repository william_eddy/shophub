package com.shophub_team1.jpa.controller;

import com.shophub_team1.jpa.controller.forms.EditShopperProfileForm;
import com.shophub_team1.jpa.entity.UserEntity;
import com.shophub_team1.jpa.miscObjects.LoggedInUserAttribute;
import com.shophub_team1.jpa.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Optional;

//user profile controller
@RequestMapping("/profile")
@Controller
public class UserProfileController extends LoggedInUserAttribute {

    @Autowired
    UserService userService;

    @Autowired
    LoyaltyCardService loyaltyCardService;

    @Autowired
    LoggedInUserService loggedInUserService;

    @Autowired
    ImageService imageService;

    @Autowired
    LoggingService loggingService;


    //a route to render a user profile template
    @GetMapping({""})
    public String profile(Model model, Model user, Model loyaltyCards, Model general){
        user.addAttribute("firstName", loggedInUserService.getFirstName());
        user.addAttribute("lastName", loggedInUserService.getLastName());
        user.addAttribute("profilePicture", loggedInUserService.getProfilePicture());
        user.addAttribute("emailAddress", loggedInUserService.getEmailAddress());
        general.addAttribute("numberOfCards",loyaltyCardService.getNumberOfLoyaltyCardsByUser(loggedInUserService.getUserId()));
        loyaltyCards.addAttribute("loyaltyCards", loyaltyCardService.getLoyaltyCardsByUser(loggedInUserService.getUserId()));

        return "profile.html";
    }

    // Mapping for the user to edit their profile
    @GetMapping("/edit")
    public String editShopperProfile(Authentication authentication, Model model) {
        EditShopperProfileForm editShopperProfileForm = new EditShopperProfileForm();
        model.addAttribute("editShopperProfileForm", editShopperProfileForm);

        Optional<String> email = Optional.of(authentication.getName());
        UserEntity userDetails = userService.getUserByEmail(email.get());

        model.addAttribute("user", userDetails);
        return "edit-profile.html";
    }

    // Mapping that is called when the edit profile form is submitted
    @PostMapping("/edit/submit")
    public String submitEditShopperProfilePost(@Valid EditShopperProfileForm editShopperProfileForm, BindingResult bindingResult, Authentication authentication, Model model) throws IOException {
        String firstName = editShopperProfileForm.getFirstName();
        String lastName = editShopperProfileForm.getLastName();
        String emailAddress = editShopperProfileForm.getEmailAddress();

        UserEntity user = loggedInUserService.getUser();

        if (bindingResult.hasErrors()) {
            System.out.println("Errors: " + bindingResult.getAllErrors());
            model.addAttribute("user", loggedInUserService.getUser());
            return "edit-profile.html";
        }else{
            loggingService.saveLog(6L,"N/A");

            System.out.println("We're updating:");
            System.out.println(user.getUserId());
            System.out.println(user.getFirstName());
            System.out.println(user.getLastName());
            System.out.println(user.getEmailAddress());

            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setEmailAddress(emailAddress);

            if(editShopperProfileForm.getProfilePicture() != null) {
                MultipartFile profilePic = editShopperProfileForm.getProfilePicture();
                user.setProfilePicture(profilePic.getOriginalFilename());
                imageService.saveImage(profilePic, "profiles/");
            }

            System.out.println("To:");
            System.out.println(user.getUserId());
            System.out.println(user.getFirstName());
            System.out.println(user.getLastName());
            System.out.println(user.getEmailAddress());

            userService.updateUser(user);
            return "redirect:/logout";
        }

    }

}
