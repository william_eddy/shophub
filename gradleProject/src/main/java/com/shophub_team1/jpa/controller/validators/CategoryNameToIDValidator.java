package com.shophub_team1.jpa.controller.validators;

import com.shophub_team1.jpa.service.AdminAddRetailerService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class CategoryNameToIDValidator implements ConstraintValidator<CategoryNameToID, String> {

    private final AdminAddRetailerService adminAddRetailerService;

    public CategoryNameToIDValidator(AdminAddRetailerService service) {
        adminAddRetailerService = service;
    }

    @Override
    public boolean isValid(String Name, ConstraintValidatorContext constraintValidatorContext) {
        return adminAddRetailerService.doesCategoryExist(Name);
    }
}
