package com.shophub_team1.jpa.controller.forms;

import com.shophub_team1.jpa.controller.validators.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;
import javax.validation.constraints.Size;

// Form validator object that is mapped to thymeleaf template

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdminAddRetailersForm{

    private Long retailerId;

    // Reasonable parameters for name size
    @Size(min = 1, max = 80, message = "The name of the business must be between 1 and 80 characters")
    private String name;

    // Reasonable max size for description
    @Size(max = 500, message = "The size of the description can not be more than 500 characters")
    private String description;

    //Calls the LogoImage validator, checking if the image given is .svg
    @LogoImage
    private MultipartFile logo;

    // Calls the backgroundImage validator, checking if the image is PNG
    @BackgroundImage
    private MultipartFile backgroundImage;

    private Integer maxPoints;

    @CategoryNameToID
    private String categoryName;

    @RetailerAddUserID
    private Long userId;

    private String websiteUrl;

}
