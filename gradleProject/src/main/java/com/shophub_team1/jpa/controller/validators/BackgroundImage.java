package com.shophub_team1.jpa.controller.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = BackgroundImageValidator.class)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface BackgroundImage {

    String message() default "The logo must be .png";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
