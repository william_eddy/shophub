package com.shophub_team1.jpa.controller.forms;

import com.shophub_team1.jpa.controller.validators.ContainsNumberConstraint;
import com.shophub_team1.jpa.controller.validators.NewEmailConstraint;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddNewUserForm {

    private Long userId;
    private Long roleId;

    @Size(min = 1, max = 50, message = "First name must be between 1 and 50 characters")
    private String firstName;

    @Size(min = 1, max = 50, message = "Last name must be between 1 and 50 characters")
    private String lastName;

    @Size(min = 1, max = 50, message = "Email Address must be between 1 and 50 characters")
    @Email
    @NewEmailConstraint
    private String emailAddress;

    @Size(min = 5, max = 50, message = "Password must be between 5 and 50 characters")
    @ContainsNumberConstraint
    private String password;

}
