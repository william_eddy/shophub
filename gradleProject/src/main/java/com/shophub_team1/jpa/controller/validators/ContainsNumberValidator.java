package com.shophub_team1.jpa.controller.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ContainsNumberValidator implements
        ConstraintValidator<ContainsNumberConstraint, String> {

    @Override
    public void initialize(ContainsNumberConstraint contactNumber) {
    }

    @Override
    public boolean isValid(String field,
                           ConstraintValidatorContext cxt) {
        return field.matches(".*\\d.*");
    }

}
