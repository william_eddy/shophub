package com.shophub_team1.jpa.controller.validators;

import com.shophub_team1.jpa.service.ImageService;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class BackgroundImageValidator implements ConstraintValidator<BackgroundImage, MultipartFile> {

    private ImageService imageService;

    public BackgroundImageValidator(ImageService service) {
        imageService = service;
    }

    @Override
    public boolean isValid(MultipartFile file, ConstraintValidatorContext constraintValidatorContext) {
        if(file != null) {
            return imageService.validateFileIsPNG(file);
        }else{
            return true;
        }
    }
}
