package com.shophub_team1.jpa.controller;

import com.shophub_team1.jpa.entity.RetailerEntity;
import com.shophub_team1.jpa.service.*;
import com.shophub_team1.jpa.miscObjects.LoggedInUserAttribute;
import com.shophub_team1.jpa.service.LoggedInUserService;
import com.shophub_team1.jpa.service.RetailerService;
import com.shophub_team1.jpa.service.UserTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

//discover controller
@Controller
@RequestMapping("/discover")
public class DiscoverController extends LoggedInUserAttribute {

    @Autowired
    UserTagService userTagService;

    @Autowired
    RetailerService retailerService;

    @Autowired
    LoggedInUserService loggedInUserService;

    @Autowired
    LoyaltyCardService loyaltyCardService;

    //renders discover home page
    @GetMapping({""})
    public String discoverHome(Model model){

        Long tagCount;
        tagCount = userTagService.countUserTagsByUser(loggedInUserService.getUserId());
        if(tagCount.intValue() < 3){
            return "redirect:/register/tags";
        }

        List<RetailerEntity> fashionAndClothing = retailerService.getRetailersByCategoryName("Fashion and Clothing");
        List<RetailerEntity> foodAndDrink = retailerService.getRetailersByCategoryName("Food and Drink");
        List<RetailerEntity> homeStore = retailerService.getRetailersByCategoryName("Home Store");

        List<List<RetailerEntity>> categories = new ArrayList<>(List.of(fashionAndClothing, foodAndDrink, homeStore));

        List<Long> joinedRetailers = loyaltyCardService.getRetailerIdsOfLoyaltyCardsByUserId(loggedInUserService.getUserId());
        System.out.println("Here are the retailer id's: ");
        joinedRetailers.forEach(System.out::println);


        model.addAttribute("loggedInRole", loggedInUserService.getRole());
        model.addAttribute("categories", categories);
        model.addAttribute("joinedRetailers", joinedRetailers);

        return "discover.html";
    }

    //adds a loyalty card to a user given a retailerId
    @PostMapping({""})
    public String sub(@RequestParam(value = "id") Long id){
        Long userId = loggedInUserService.getUserId();
        System.out.println("userId" + userId + "retailerId" + id);
        loyaltyCardService.save(userId, id, 0, 0);
        return "redirect:/profile";
    }

    //renders the search result from the navigation bar
    @GetMapping("/{context}/{search}")
    public String showRetailerProfile(
            @PathVariable(value="context") String context,
            @PathVariable(value="search") String search,
            Model model) {

        search = decodeUrl(search);

        if(context.equals("category")){
            List<RetailerEntity> retailers = retailerService.getRetailersByCategoryName(search);
            model.addAttribute("retailers",retailers);
            model.addAttribute("title",search);
        }else if(context.equals("tag")){
            List<RetailerEntity> retailers = retailerService.getRetailersByTag(search);
            model.addAttribute("retailers",retailers);
            model.addAttribute("title","#" + search);
        }

        List<Long> joinedRetailers = loyaltyCardService.getRetailerIdsOfLoyaltyCardsByUserId(loggedInUserService.getUserId());
        model.addAttribute("joinedRetailers", joinedRetailers);
        return "filteredView";
    }

    //renders the retailer profile
    @GetMapping("/retailer/profile/{retailerName}")
    public String showRetailerProfile(
            @PathVariable(value="retailerName") String retailerName,
            Model model) {

        RetailerEntity retailer = retailerService.getRetailerByName(decodeUrl(retailerName));
        model.addAttribute("retailer", retailer);

        return "retailer-profile.html";
    }

    private String decodeUrl(String url){
        return URLDecoder.decode(url, StandardCharsets.UTF_8);
    }


}
