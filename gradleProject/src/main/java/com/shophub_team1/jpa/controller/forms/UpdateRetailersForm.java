package com.shophub_team1.jpa.controller.forms;

import com.shophub_team1.jpa.controller.validators.*;
import com.shophub_team1.jpa.entity.RetailerEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateRetailersForm {

    private Long retailerId;

    // Reasonable parameters for name size
    @Size(min = 1, max = 80, message = "The name of the business must be between 1 and 80 characters")
    @SanitiseConstraint
    private String name;

    // Reasonable max size for description
    @Size(max = 600, message = "The size of the description can not be more than 500 characters")
    private String description;

    //Calls the LogoImage validator, checking if the image given is .svg
    @LogoImage
    private MultipartFile logo;

    // Calls the backgroundImage validator, checking if the image is PNG
    @BackgroundImage
    private MultipartFile backgroundImage;

    private Integer maxPoints;

    @CategoryNameToID
    private String categoryName;

    @RetailerAddUserID
    private Long userId;

    private String websiteUrl;

    private String backgroundImagePath;

    private String logoImagePath;

    private Long categoryId;

    public void updateRetailersFormPreLoad(RetailerEntity retailerEntity){

        // Im not using this() here as it requires them to be in the set order

        retailerId = retailerEntity.getRetailerId();
        name = retailerEntity.getName();
        description = retailerEntity.getDescription();
        backgroundImagePath = retailerEntity.getBackgroundImage();
        maxPoints = retailerEntity.getMaxPoints();
        logoImagePath = retailerEntity.getLogo();
        categoryName = retailerEntity.getCategory().getName();
        userId = retailerEntity.getUserId();
        websiteUrl = retailerEntity.getWebsiteUrl();
        categoryId = retailerEntity.getCategory().getCategoryId();

    }
}
