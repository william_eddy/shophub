package com.shophub_team1.jpa.controller.validators;

import com.shophub_team1.jpa.service.ImageService;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class LogoImageValidator implements ConstraintValidator<LogoImage, MultipartFile> {

    private final ImageService imageService;

    public LogoImageValidator(ImageService service) {
        imageService = service;
    }

    @Override
    public boolean isValid(MultipartFile file, ConstraintValidatorContext constraintValidatorContext) {
        return imageService.validateFileIsSVG(file);
    }
}
