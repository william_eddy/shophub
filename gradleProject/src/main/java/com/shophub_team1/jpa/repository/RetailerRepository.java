package com.shophub_team1.jpa.repository;

import com.shophub_team1.jpa.entity.CategoryEntity;
import com.shophub_team1.jpa.entity.RetailerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import javax.transaction.Transactional;
import java.util.List;

//repository for retailers
@Repository
public interface RetailerRepository extends JpaRepository<RetailerEntity, Long> {

    @Query("select r from RetailerEntity r WHERE r.category.name = ?1")
    List<RetailerEntity> getRetailersByCategory(String categoryName);

    @Query("select r from RetailerEntity r WHERE r.name = ?1")
    RetailerEntity getRetailerByName(String name);

    //returns category names like a search parameter
    @Query("select r.name from RetailerEntity r WHERE UPPER(r.name) like UPPER(concat('%', ?1,'%'))")
    List<String> searchRetailerNames(String search);

    List<RetailerEntity> findByTags_name(String aName);

    @Transactional
    @Modifying
    @Query("update RetailerEntity u set u.name = ?1, u.description = ?2, u.logo = ?3, u.backgroundImage = ?4, u.maxPoints = ?5, u.userId = ?6, u.category = ?7, u.websiteUrl = ?8 where u.retailerId = ?9")
    void updateRetailer(String name, String description, String logo, String backgroundImage, Integer maxPoints, Long userId, CategoryEntity categoryEntity, String websiteUrl, Long retailerId);


}
