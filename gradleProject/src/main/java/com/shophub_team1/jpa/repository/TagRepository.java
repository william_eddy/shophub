package com.shophub_team1.jpa.repository;

import com.shophub_team1.jpa.entity.TagEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

//repository for tags
@Repository
public interface TagRepository extends JpaRepository<TagEntity, Long> {

    //returns category names like a search parameter
    @Query("select t.name from TagEntity t WHERE t.name like %?1%")
    List<String> searchTagNames(String search);

    List<TagEntity> findAll();

}
