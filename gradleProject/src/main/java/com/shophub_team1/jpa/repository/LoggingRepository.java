package com.shophub_team1.jpa.repository;

import com.shophub_team1.jpa.entity.LoggingEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

//repository for logs
@Repository
public interface LoggingRepository extends JpaRepository<LoggingEntity, Long> {
    LoggingEntity save(LoggingEntity aLog);
}


