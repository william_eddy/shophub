package com.shophub_team1.jpa.repository;

import com.shophub_team1.jpa.entity.CategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

//repository for categories
@Repository
public interface CategoryRepository extends JpaRepository<CategoryEntity, Long> {

    @Query("select c from CategoryEntity c WHERE c.name= ?1")
    CategoryEntity getCategoryByName(String categoryName);

    @Query("select c.categoryId from CategoryEntity c WHERE c.name= ?1")
    Optional<Long> getCategoryIDByName(String categoryName);

    //returns category names like a search parameter
    @Query("select c.name from CategoryEntity c WHERE c.name like %?1%")
    List<String> searchCategoryNames(String search);


}
