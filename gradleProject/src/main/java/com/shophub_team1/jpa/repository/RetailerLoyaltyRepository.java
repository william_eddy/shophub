package com.shophub_team1.jpa.repository;

import com.shophub_team1.jpa.entity.RetailerLoyaltyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

//repository for the retailer_loyalty join table
@Repository
public interface RetailerLoyaltyRepository extends JpaRepository<RetailerLoyaltyEntity, Long> {

    @Query("select l from RetailerLoyaltyEntity l where l.userId = ?1 order by l.retailer.retailerId ASC")
    List<RetailerLoyaltyEntity> findLoyaltyCardsByUser(Long userId);

    @Query("select COUNT(l) from RetailerLoyaltyEntity l where l.userId = ?1")
    Long findNumberOfLoyaltyCardsByUser(Long userId);

    @Query("select r.retailer.retailerId from RetailerLoyaltyEntity r where r.userId = ?1")
    List<Long> findSignedUpRetailersByUserId(Long userId);

    RetailerLoyaltyEntity save(RetailerLoyaltyEntity aLoyaltyCardSub);


}
