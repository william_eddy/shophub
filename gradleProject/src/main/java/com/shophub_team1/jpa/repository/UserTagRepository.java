package com.shophub_team1.jpa.repository;

import com.shophub_team1.jpa.entity.UserTagEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

//repository for user tags
@Repository
public interface UserTagRepository extends JpaRepository<UserTagEntity, Long> {
    @Query("SELECT COUNT(u) FROM UserTagEntity u WHERE u.userId=?1")
    long countUserTagsByUser(long userId);
}
