package com.shophub_team1.jpa.repository;

import com.shophub_team1.jpa.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import javax.transaction.Transactional;
import java.util.Optional;

//repository for users
@Repository
@Transactional
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    @Modifying
    @Query(value="call UPDATE_USER(?1, ?2, ?3, ?4, ?5, ?6)", nativeQuery = true)
    void updateUser(Long userId, String firstName, String lastName, String emailAddress, String password, String profilePicture);

    Optional<UserEntity> findByEmailAddress(String emailAddress);

    @Query("select u from UserEntity u where u.emailAddress = ?1")
    UserEntity getUserByEmailAddress(String emailAddress);


}
