package com.shophub_team1.jpa.miscObjects;

import lombok.AllArgsConstructor;
import lombok.Data;

//data transfer object for the logged-in user API
@AllArgsConstructor
@Data
public class UserNavbar {
    private String firstName;
    private String profilePic;
}
