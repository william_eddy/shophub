package com.shophub_team1.jpa.miscObjects;

import com.shophub_team1.jpa.service.LoggedInUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;

//data transfer object for the data of a logged-in user
public class LoggedInUserAttribute {

    //business logic is autowired in from services

    @Autowired
    LoggedInUserService loggedInUserService;

    @ModelAttribute
    public void addLoggedInUser(Model model) {
        model.addAttribute("loggedInUser", loggedInUserService.getUser());
    }

}
