package com.shophub_team1.jpa.miscObjects;

import lombok.AllArgsConstructor;
import lombok.Data;

//data transfer object for a search result
@AllArgsConstructor
@Data
public class SearchResult {
    private String contextType;
    private String name;
}
