package com.shophub_team1.jpa.miscObjects;

import com.structurizr.model.Component;

public class DuplicateComponentStrategy implements com.structurizr.analysis.DuplicateComponentStrategy {

    @Override
    public Component duplicateComponentFound(Component component, String name, String type, String description, String technology) {
        return component;
    }

}
