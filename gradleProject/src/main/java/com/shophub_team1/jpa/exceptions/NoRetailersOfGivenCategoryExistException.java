package com.shophub_team1.jpa.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

//given there are no retailers of a given category, this error is served
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NoRetailersOfGivenCategoryExistException extends RuntimeException {
    public NoRetailersOfGivenCategoryExistException(String s) {
        super(s);
    }
}
