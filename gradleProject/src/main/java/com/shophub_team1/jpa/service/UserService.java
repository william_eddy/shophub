package com.shophub_team1.jpa.service;

import com.shophub_team1.jpa.entity.UserEntity;
import com.shophub_team1.jpa.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//service for users
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    //get a user given an emailAddress
    public UserEntity getUserByEmail(String emailAddress) {
        return userRepository.findByEmailAddress(emailAddress).get();
    }

    //method to take all of the information required and update an existing user in the database
    public void updateUser(UserEntity user) {
        userRepository.updateUser(user.getUserId(), user.getFirstName(), user.getLastName(), user.getEmailAddress(), user.getPassword(), user.getProfilePicture());
    }

    //check if the ID exists for user
    public Boolean doesIdExist(Long ID){
        return userRepository.existsById(ID);
    }

    //check if the email exists for user
    public Boolean isEmailAddressNew(String emailAddress){
        if(userRepository.findByEmailAddress(emailAddress).isPresent()){
            return false;
        }
        return true;
    }

    //save a new user into the database
    public void save(String firstName, String lastName, String emailAddress, String password, String profilePicture){
        UserEntity newUser = new UserEntity(firstName,lastName,emailAddress,password,profilePicture);
        userRepository.save(newUser);
    }

}
