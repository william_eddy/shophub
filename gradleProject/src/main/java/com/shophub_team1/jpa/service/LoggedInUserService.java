package com.shophub_team1.jpa.service;

import com.shophub_team1.jpa.entity.UserEntity;
import com.shophub_team1.jpa.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

//service to return data about the logged in user
@Service
public class LoggedInUserService {

    @Autowired
    UserRepository userRepository;

    //returns the userId of the logged-in user
    public Long getUserId(){
        if (getEmailAddress()==null){
            return null;
        }
        return userRepository.findByEmailAddress(getEmailAddress()).get().getUserId();
    }

    //returns the firstName of the logged-in user
    public String getFirstName(){
        if (getEmailAddress()==null){
            return null;
        }
        return userRepository.findById(getUserId()).get().getFirstName();
    }

    //returns the lastName of the logged-in user
    public String getLastName(){
        if (getEmailAddress()==null){
            return null;
        }
        return userRepository.findById(getUserId()).get().getLastName();
    }

    //returns the profilePicture of the logged-in user
    public String getProfilePicture(){
        if (getEmailAddress()==null){
            return null;
        }
        return userRepository.findById(getUserId()).get().getProfilePicture();
    }

    //returns the emailAddress of the logged-in user
    public String getEmailAddress() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication == null || authentication.getName().equals("anonymousUser")) {
            return null;
        }else {
            return authentication.getName();
        }
    }

    //returns the role name of the logged-in user
    public String getRole(){
        if (getEmailAddress()==null){
            return null;
        }
        return userRepository.findById(getUserId()).get().getRole().getName();
    }

    //returns the user as an entity of the logged-in user
    public UserEntity getUser(){
        if (getEmailAddress()==null){
            return null;
        }
        return userRepository.getUserByEmailAddress(getEmailAddress());
    }

}

