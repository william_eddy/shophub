package com.shophub_team1.jpa.service;

import com.shophub_team1.jpa.entity.TagEntity;
import com.shophub_team1.jpa.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;

//service for tags
@Service
public class TagService {

    @Autowired
    private TagRepository tagRepository;

    //get all tags
    public List<TagEntity> getTags(){
        return tagRepository.findAll();
    }
}
