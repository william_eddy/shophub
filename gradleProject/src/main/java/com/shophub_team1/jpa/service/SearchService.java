package com.shophub_team1.jpa.service;

import com.shophub_team1.jpa.miscObjects.SearchResult;
import com.shophub_team1.jpa.repository.CategoryRepository;
import com.shophub_team1.jpa.repository.RetailerRepository;
import com.shophub_team1.jpa.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

//service for searches
@Service
public class SearchService {

    @Autowired
    private RetailerRepository retailerRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private TagRepository tagRepository;

    //get the search results for retailer names, tags and categories given a search string
    public List<SearchResult> getAllSearchResults(String search){
        List<SearchResult> searchResults = new ArrayList<>();
        List<String> retailerSearchResults = retailerRepository.searchRetailerNames(search);
        List<String> categorySearchResults = categoryRepository.searchCategoryNames(search);
        List<String> tagSearchResults = tagRepository.searchTagNames(search);

        for (int i = 0; i < retailerSearchResults.size(); i++) {
            SearchResult retailer = new SearchResult("retailer",retailerSearchResults.get(i));
            searchResults.add(retailer);
        }

        for (int i = 0; i < categorySearchResults.size(); i++) {
            SearchResult category = new SearchResult("category",categorySearchResults.get(i));
            searchResults.add(category);
        }

        for (int i = 0; i < tagSearchResults.size(); i++) {
            SearchResult tag = new SearchResult("tag",tagSearchResults.get(i));
            searchResults.add(tag);
        }

        return searchResults;
    }
}
