package com.shophub_team1.jpa.service;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.Objects;

//hashing service (not in use but ready to wire)
public class HashingService {

    // Function takes an input of a password and generates a hash
    public byte[] hashString(String item, byte[] salt) throws InvalidKeySpecException, NoSuchAlgorithmException {

        KeySpec spec = new PBEKeySpec(item.toCharArray(), salt, 65536, 128);
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] hash = factory.generateSecret(spec).getEncoded();
        return hash;
    }

    public String toHashString(byte[] Byte){

        StringBuilder sb = new StringBuilder();
        for (byte b : Byte)
            sb.append(String.format("%02x", b));
        return sb.toString();

    }

    // Return a %02x back into bytes, found here:
    // https://stackoverflow.com/questions/25206289/how-to-convert-value-back-from-02x-in-java

    public byte[] toBytes(String string){
        int len = string.length();
        byte[] storage = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            storage[i / 2] = (byte) ((Character.digit(string.charAt(i), 16) << 4) + Character.digit(string.charAt(i+1), 16));
        }
        return storage;
    }

    public byte[] getNewSalt(){
        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[16];
        random.nextBytes(salt);
        return salt;
    }

    public String getCurrentSalt(String completeHash){

        String[] items = completeHash.split(":");
        return items[0];
    }

    public String getCurrentHash(String completeHash){

        String[] items = completeHash.split(":");
        return items[1];
    }

    public String generateHashForDB(byte[] Salt, byte[] hash) {

        StringBuilder sb = new StringBuilder();
        sb.append(toHashString(Salt));
        sb.append(":");
        sb.append(toHashString(hash));
        return sb.toString();
    }

    public boolean authenticateUserHash(String userPassword, String databaseHash) throws InvalidKeySpecException, NoSuchAlgorithmException {

        byte[] ourSalt = toBytes(getCurrentSalt(databaseHash));
        System.out.println(Arrays.toString(ourSalt));
        String ourHash = getCurrentHash(databaseHash);
        String theirHashWithOurSalt = toHashString(hashString(userPassword, ourSalt));
        System.out.println(ourHash);
        System.out.println(theirHashWithOurSalt);
        return Objects.equals(ourHash, theirHashWithOurSalt);


    }

    // Explanation

    //Generate a new salt here
    //byte[] salt = userService.getNewSalt();

    //Generate a hash not encoded for the database (its in raw byte format)
    //byte[] dbhashraw = userService.hashString("password", salt));

    //Generate database friendly hash string
    //String dbhashactual = userService.generateHashForDB(salt, dbhash);

    // Use this string to compare to other passwords below -
    // System.out.println(userService.authenticateUserHash("password", dbhash)); - comes as true
    // System.out.println(userService.authenticateUserHash("passdwq", dbhash)); - comes as false


}
