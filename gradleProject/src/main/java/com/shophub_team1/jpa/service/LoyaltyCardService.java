package com.shophub_team1.jpa.service;

import com.shophub_team1.jpa.entity.RetailerLoyaltyEntity;
import com.shophub_team1.jpa.repository.RetailerLoyaltyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

//service for user loyalty cards
@Service
public class LoyaltyCardService {

    @Autowired
    private RetailerLoyaltyRepository retailerLoyaltyRepository;

    //get the loyalty cards for a given user
    public List<RetailerLoyaltyEntity> getLoyaltyCardsByUser(Long userId) {
        return retailerLoyaltyRepository.findLoyaltyCardsByUser(userId);
    }

    //get the number of loyalty cards for a given user
    public Long getNumberOfLoyaltyCardsByUser(Long userId) {
        return retailerLoyaltyRepository.findNumberOfLoyaltyCardsByUser(userId);
    }

    //get the retailer ids of loyalty cards given a userId
    public List<Long> getRetailerIdsOfLoyaltyCardsByUserId(Long userId){
        return retailerLoyaltyRepository.findSignedUpRetailersByUserId(userId);
    }

    //save a new loyalty card for a user
    public void save(Long id, Long userId, Integer points, Integer rewardsClaimed) {
        RetailerLoyaltyEntity newSub;
        newSub = new RetailerLoyaltyEntity(id, userId, points, rewardsClaimed);
        retailerLoyaltyRepository.save(newSub);

    }
}

