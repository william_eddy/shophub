package com.shophub_team1.jpa.service;

import com.shophub_team1.jpa.controller.forms.UpdateRetailersForm;
import com.shophub_team1.jpa.entity.CategoryEntity;
import com.shophub_team1.jpa.entity.RetailerEntity;
import com.shophub_team1.jpa.repository.RetailerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.Optional;

//update retailer service
@Service
public class UpdateRetailerService {

    @Autowired
    RetailerRepository retailerRepository;

    @Autowired
    RetailerService retailerService;

    @Autowired
    ImageService imageService;

    @Autowired
    AdminAddRetailerService adminAddRetailerService;

    //update retailers using a given RetailerEntity
    public void useEntityToUpdateRetailer(RetailerEntity retailerEntity){
        retailerRepository.updateRetailer(retailerEntity.getName(),
                retailerEntity.getDescription(),
                retailerEntity.getLogo(),
                retailerEntity.getBackgroundImage(),
                retailerEntity.getMaxPoints(),
                retailerEntity.getUserId(),
                retailerEntity.getCategory(),
                retailerEntity.getWebsiteUrl(),
                retailerEntity.getRetailerId());
    }

    //create RetailerEntity from update retailers form data
    public RetailerEntity formToEntity(UpdateRetailersForm updateRetailersForm) throws IOException {

        Optional<Long> categoryID = adminAddRetailerService.getCategoryIdByName(updateRetailersForm.getCategoryName());

        updateRetailersForm = handleImageFromUpdateForm(updateRetailersForm);

        return new RetailerEntity(
                updateRetailersForm.getRetailerId(),
                updateRetailersForm.getName(),
                updateRetailersForm.getDescription(),
                updateRetailersForm.getLogoImagePath(),
                updateRetailersForm.getBackgroundImagePath(),
                updateRetailersForm.getMaxPoints(),
                updateRetailersForm.getUserId(),
                new CategoryEntity(categoryID.get(), updateRetailersForm.getCategoryName()),
                updateRetailersForm.getWebsiteUrl());

    }

    //process image from update retailers form
    public UpdateRetailersForm handleImageFromUpdateForm(UpdateRetailersForm updateRetailersForm) throws IOException {

        if (imageService.doesMultipartContainImage(updateRetailersForm.getLogo()).equals(true)){
            System.out.println("Found image for logo");
            imageService.saveImage(updateRetailersForm.getLogo(), "retailers/logos/");
            updateRetailersForm.setLogoImagePath(updateRetailersForm.getLogo().getName());
        }
        ;
        if (imageService.doesMultipartContainImage(updateRetailersForm.getBackgroundImage()).equals(true)){
            System.out.println("Found image for background");
            imageService.saveImage(updateRetailersForm.getBackgroundImage(), "retailers/backgrounds/");
            updateRetailersForm.setBackgroundImagePath(updateRetailersForm.getBackgroundImage().getName());
        }
        ;
        System.out.println("Logo image is ");
        System.out.println(updateRetailersForm.getLogoImagePath());
        return updateRetailersForm;
    }

    //entry point for updating a retailer
    public void formToDatabase(UpdateRetailersForm updateRetailersForm) throws IOException {
        useEntityToUpdateRetailer(formToEntity(updateRetailersForm));
    }
}





