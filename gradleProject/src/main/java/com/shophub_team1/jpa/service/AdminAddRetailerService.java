package com.shophub_team1.jpa.service;

import com.shophub_team1.jpa.entity.RetailerEntity;
import com.shophub_team1.jpa.repository.CategoryRepository;
import com.shophub_team1.jpa.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;

//service for adding new retailers to the database
@Service
public class AdminAddRetailerService{

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    UserRepository userRepository;

    //check if the name given by form is linked to an ID
    public Boolean doesCategoryExist(String name){
        Optional<Long> a = categoryRepository.getCategoryIDByName(name);
        if (a.isPresent()){
            return Boolean.TRUE;
            }else{
            return Boolean.FALSE;
        }
    }

    //converts given data into entity
    public RetailerEntity toRetailer(String name, String description, String logo, String backgroundImage,
                                     Integer maxPoints, Long categoryId, Long userId, String websiteUrl) {
        return new RetailerEntity(name, description, logo, backgroundImage, maxPoints, categoryId, userId, websiteUrl);
    }

    //gets category ID by sending a name
    public Optional<Long> getCategoryIdByName(String name) {
        return categoryRepository.getCategoryIDByName(name);
    }
}
