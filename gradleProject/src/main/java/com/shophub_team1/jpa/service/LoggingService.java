package com.shophub_team1.jpa.service;

import com.shophub_team1.jpa.entity.LoggingEntity;
import com.shophub_team1.jpa.repository.LoggingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//service to log significant system events
@Service
public class LoggingService {

    @Autowired
    private LoggingRepository loggingRepository;

    @Autowired
    private LoggedInUserService loggedInUserService;

    //saves a new log into the database
    public void saveLog(Long eventID, String details){
        LoggingEntity newLog;
        loggedInUserService.getEmailAddress();

        if (loggedInUserService.getEmailAddress() != null) {
            newLog = new LoggingEntity(eventID, details, loggedInUserService.getEmailAddress());
        }else{
            newLog = new LoggingEntity(eventID, details, "N/A");
        }
        loggingRepository.save(newLog);
    }
}
