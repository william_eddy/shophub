package com.shophub_team1.jpa.service;

import com.shophub_team1.jpa.entity.RetailerEntity;
import com.shophub_team1.jpa.repository.RetailerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

//service for retailers
@Service
public class RetailerService {

    @Autowired
    private RetailerRepository retailerRepository;

    //get retailers by a given category name
    public List<RetailerEntity> getRetailersByCategoryName(String categoryName){
        return retailerRepository.getRetailersByCategory(categoryName);
    }

    //get retailers by a given tag
    public List<RetailerEntity> getRetailersByTag(String tag){
        List<RetailerEntity> retailers = retailerRepository.findByTags_name(tag);
        return retailers;
    }

    //get a retailer by name
    public RetailerEntity getRetailerByName(String retailerName){
        return retailerRepository.getRetailerByName(retailerName);
    }

    //save a new retailer into the database
    public void save(RetailerEntity retailerEntity){
        retailerRepository.save(retailerEntity);
    }

}
