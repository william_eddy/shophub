package com.shophub_team1.jpa.service;

import com.shophub_team1.jpa.entity.UserTagEntity;
import com.shophub_team1.jpa.repository.UserTagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//service for user tags
@Service
public class UserTagService {

    @Autowired
    private UserTagRepository userTagRepository;

    //save a new user tag into the database
    public void save(Long userId, Long tagId){
        UserTagEntity userTagEntity = new UserTagEntity(userId, tagId);
        userTagRepository.save(userTagEntity);
    }

    //count the number of tags for a given userId
    public long countUserTagsByUser(long userId){
        return userTagRepository.countUserTagsByUser(userId);
    }
}
