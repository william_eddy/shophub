package com.shophub_team1.jpa.service;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

//service for uploading images to the server
@Service
public class ImageService {

    //saves an image to the server
    public void saveImage(MultipartFile file, String Location) throws IOException {

        String temporaryPath = (System.getProperty("user.dir") + "/temporaryMover");
        File folder = new File(temporaryPath);

        if (!folder.exists()) {
            folder.mkdir();
        }

        File tempFile = new File(System.getProperty("user.dir") + "/temporaryMover/tempfile.png");
        file.transferTo(tempFile);
        tempFile.renameTo(new File(System.getProperty("user.dir") + "/src/main/resources/static/images/" + Location + file.getOriginalFilename().toLowerCase()));
    }

        /* Given a multipart file, its original file name is found via get method.
    FilenameUtils is imported from apache and its getExtension
    method is used to get the extension, which is then checked to be svc
    If it is svg, it returns true */
    public Boolean validateFileIsSVG(MultipartFile multipartFile){
        if (multipartFile.isEmpty()){
            return Boolean.TRUE;
        }
        System.out.println(FilenameUtils.getExtension(multipartFile.getOriginalFilename()));
        if (Objects.equals(FilenameUtils.getExtension(multipartFile.getOriginalFilename()), "svg")){
            return Boolean.TRUE;
        }else{
            return Boolean.FALSE;
        }
    }

    /* Given a multipart file, its original file name is found via get method.
    FilenameUtils is imported from apache and its getExtension
    method is used to get the extension, which is then checked to be png
    If it is svg, it returns true */
    public Boolean validateFileIsPNG(MultipartFile multipartFile){
        if (multipartFile.isEmpty()){
            return Boolean.TRUE;
        }
        System.out.println(FilenameUtils.getExtension(multipartFile.getOriginalFilename()));
        if ((Objects.equals(FilenameUtils.getExtension(multipartFile.getOriginalFilename()).toLowerCase(), "png"))){
            return Boolean.TRUE;
        }else{
            return Boolean.FALSE;
        }
    }

    //checks whether file is an image
    public Boolean doesMultipartContainImage(MultipartFile multipartFile){
        if (multipartFile.getOriginalFilename().equals("")){
            return false;
        }
        return true;
    }

}
