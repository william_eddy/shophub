-- Creating the user for shophub
CREATE USER IF NOT EXISTS 'ShopHub'@'localhost' IDENTIFIED BY 'dvhjuyt567yhb-%mkuytfdfn';
GRANT SELECT, UPDATE, INSERT ON shophub.users TO 'ShopHub'@'localhost';
GRANT SELECT, UPDATE, INSERT ON shophub.categories TO 'ShopHub'@'localhost';
GRANT SELECT, UPDATE, INSERT ON shophub.retailer_loyalty TO 'ShopHub'@'localhost';
GRANT SELECT, INSERT ON shophub.retailer_social_media TO 'ShopHub'@'localhost';
GRANT SELECT, UPDATE, INSERT ON shophub.retailer_tags TO 'ShopHub'@'localhost';
GRANT SELECT, UPDATE, INSERT ON shophub.retailers TO 'ShopHub'@'localhost';
GRANT SELECT, UPDATE ON shophub.roles TO 'ShopHub'@'localhost';
GRANT SELECT, INSERT ON shophub.social_media_platforms TO 'ShopHub'@'localhost';
GRANT SELECT, UPDATE, INSERT ON shophub.tags TO 'ShopHub'@'localhost';
GRANT INSERT, SELECT ON shophub.logging TO 'ShopHub'@'localhost';
GRANT INSERT, SELECT ON shophub.events TO 'ShopHub'@'localhost';
GRANT SELECT, UPDATE, INSERT ON shophub.user_tags TO 'ShopHub'@'localhost';
GRANT EXECUTE ON PROCEDURE UPDATE_USER TO 'ShopHub'@'localhost';