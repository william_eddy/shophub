SET FOREIGN_KEY_CHECKS=0;

-- Category Inserts

INSERT INTO `categories` (`category_id`, `name`) VALUES
	(1, 'Food and Drink'),
	(2, 'Fashion and Clothing'),
	(3, 'Distillery'),
	(4, 'Home Store');


-- Retailer Inserts

INSERT INTO `retailers` (`retailer_id`, `name`, `description`, `logo`, `background_image`, `max_points`, `category_id`, `user_id`, `website_url`) VALUES
	(1,'Levis', NULL, 'levi.svg', 'leviBg.png', 5, 2, 3, 'www.levis.com'),
	(2,'Hugo Boss', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum.', 'hugoBoss.svg', 'hugoBossBg.png', 5, 2, 3, 'www.hugoboss.com/uk/home'),
	(3,'ASOS', NULL, 'asos.svg', 'asosBg.png', 5, 2, 3, 'www.asos.co.uk'),
	(4,'Five Guys', NULL, 'fiveGuys.svg', 'fiveGuysBg.png', 5, 1, 3, 'www.fiveguys.co.uk'),
	(5,'Benettis Coffee', NULL, 'benettis.svg', 'benettisBg.png', 5, 1, 3, 'www.instagram.com/benettiscoffee/?hl=en'),
	(6,'Warners', NULL, 'warners.svg', 'warnersBg.png', 5, 1, 3, 'www.warnersdistillery.com/'),
	(7,'Ye Old Sweet Shop', NULL, 'yeOldSweetShop.svg', 'yeOldSweetShopBg.png', 5, 1, 3, 'www.oldestsweetshop.co.uk/'),
	(8,'Mary and Me', NULL, 'maryAndMe.svg', 'maryAndMeBg.png', 5, 4, 3, 'www.facebook.com/meandmary/'),
	(9,'Vintage Co', NULL, 'vintageCo.svg', 'vintageCoBg.png', 5, 2, 3, 'www.vintage.co.uk/');


INSERT INTO `retailer_loyalty` (`retailer_loyalty_id`, `user_id`, `retailer_id`, `points`, `rewards_claimed`) VALUES
    (1, 3, 6, 2, 0);

INSERT INTO `retailer_loyalty` (`retailer_loyalty_id`, `user_id`, `retailer_id`, `points`, `rewards_claimed`) VALUES
    (2, 3, 5, 0, 0);

INSERT INTO `retailer_loyalty` (`retailer_loyalty_id`, `user_id`, `retailer_id`, `points`, `rewards_claimed`) VALUES
    (3, 3, 3, 2, 0);

INSERT INTO `retailer_loyalty` (`retailer_loyalty_id`, `user_id`, `retailer_id`, `points`, `rewards_claimed`) VALUES
    (4, 3, 1, 0, 0);

INSERT INTO `retailer_loyalty` (`retailer_loyalty_id`, `user_id`, `retailer_id`, `points`, `rewards_claimed`) VALUES
    (5, 1, 1, 0, 0);

INSERT INTO `retailer_loyalty` (`retailer_loyalty_id`, `user_id`, `retailer_id`, `points`, `rewards_claimed`) VALUES
    (6, 1, 2, 0, 0);

INSERT INTO `retailer_loyalty` (`retailer_loyalty_id`, `user_id`, `retailer_id`, `points`, `rewards_claimed`) VALUES
    (7, 1, 3, 0, 0);



INSERT INTO `retailer_social_media` (`retailer_social_media_id`, `retailer_id`, `social_media_platform_id`, `social_media_url`) VALUES
	(1, 2, 1, 'www.facebook.com/hugoboss'),
	(2, 2, 2, 'www.twitter.com/hugoboss'),
	(3, 2, 3, 'www.instagram.com/hugoboss');


INSERT INTO `retailer_tags` (`retailer_id`, `tag_id`) VALUES
	(1, 1),
	(1, 2),
	(1, 3),
	(1, 4),
	(1, 5),

	(2, 6),
	(2, 7),
	(2, 8),
	(2, 9),
	(2, 10),

	(3, 11),
	(3, 12),
	(3, 1),
	(3, 13),
	(3, 14),
	(3, 15),

	(4, 16),
	(4, 17),
	(4, 18),
	(4, 19),
	(4, 20),
	(4, 21),
	(4, 22),

	(5, 23),
	(5, 22),
	(5, 24),
	(5, 25),

	(6, 26),
	(6, 27),
	(6, 28),
	(6, 29),
	(6, 30),
	(6, 31),

	(7, 32),
	(7, 16),
	(7, 24),
	(7, 22),
	(7, 35),

	(8, 33),
	(8, 34),
	(8, 35),
	(8, 36),
	(8, 24),

	(9, 25),
	(9, 1);


INSERT INTO `roles` (`role_id`, `name`) VALUES
	(1, 'Admin'),
	(2, 'Retailer'),
	(3, 'Shopper');
    
INSERT INTO `user_tags` (`tag_id`, `user_id`) VALUES
	(1, 1),
	(2, 1),
	(3, 1);


INSERT INTO `social_media_platforms` (`social_media_platform_id`, `name`, `logo_filename`) VALUES
	(1, 'Facebook', 'facebook.png'),
	(2, 'Twitter', 'twitter.png'),
	(3, 'Instagram', 'instagram.png');


INSERT INTO `tags` (`tag_id`, `name`) VALUES
	(1, 'clothing'),
	(2, 'jeans'),
	(3, 'menswear'),
	(4, 'hats'),
	(5, 'jumpers'),
	(6, 'formalwear'),
	(7, 'suits'),
	(8, 'shoes'),
	(9, 'professional'),
	(10, 'workwear'),
	(11,'trendy'),
	(12, 'boots'),
	(13, 'outerwear'),
	(14, 'style'),
	(15, 'casual'),
	(16, 'food'),
	(17, 'drink'),
	(18, 'burgers'),
	(19, 'fastfood'),
	(20, 'fries'),
	(21, 'milkshakes'),
	(22, 'vegan'),
	(23, 'coffee'),
	(24, 'independent'),
	(25, 'sustainable'),
	(26, 'gin'),
	(27, 'distillery'),
	(28, 'presents'),
	(29, 'alcohol'),
	(30, 'drinks'),
	(31, 'tonic'),
	(32, 'sweets'),
	(33, 'design'),
	(34, 'decor'),
	(35, 'modern'),
	(36, 'interior');


INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `email_address`, `password`, `profile_picture`, `role_id`) VALUES
	(1, 'William', 'Eddy', 'william@eddy.com', '12345', 'will.png', 1),
	(2, 'Jared', 'Brokenshire', 'jared@brokenshire.com', '12345', 'jared.png', 2),
	(3, 'Haroon', 'Mohammed', 'haroon@mohammed.com', '12345', 'haroon.png', 3);

INSERT INTO `events`(`event_id`,`name`) VALUES
	 (1, 'Other'),
	 (2, 'Successful login'),
	 (3, 'Unsuccessful login'),
	 (4, 'New retailer added'),
	 (5, 'Retailer edited'),
	 (6, 'Account details changed');

SET FOREIGN_KEY_CHECKS=1;