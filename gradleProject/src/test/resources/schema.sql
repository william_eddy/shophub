SET FOREIGN_KEY_CHECKS=0;

-- Dumping structure for table shophub.roles
CREATE TABLE `roles` (
	`role_id` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(50) NOT NULL,
	PRIMARY KEY (`role_id`)
);

-- Dumping structure for table shophub.users
CREATE TABLE `users` (
	`user_id` int(11) NOT NULL AUTO_INCREMENT,
	`first_name` varchar(50) NOT NULL DEFAULT '0',
	`last_name` varchar(50) NOT NULL DEFAULT '0',
	`email_address` varchar(50) NOT NULL DEFAULT '0',
	`password` varchar(50) NOT NULL DEFAULT '0',
	`profile_picture` varchar(50) DEFAULT '0',
	`role_id` int(11) NOT NULL DEFAULT 3,
	PRIMARY KEY (`user_id`),
	CONSTRAINT `role_id_users` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`)
);


-- Dumping structure for table shophub.categories
CREATE TABLE `categories` (
    `category_id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(50) NOT NULL DEFAULT '0',
    PRIMARY KEY (`category_id`)
    );

-- Dumping structure for table shophub.retailers
CREATE TABLE `retailers` (
    `retailer_id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(50) NOT NULL UNIQUE,
    `description` TEXT DEFAULT NULL,
    `logo` varchar(50) NULL,
    `background_image` varchar(50) NULL,
    `max_points` int(11) DEFAULT 0,
    `category_id` int(11) DEFAULT 0,
    `user_id` int(11) NOT NULL DEFAULT 0,
    `website_url` varchar(50) DEFAULT NULL,
    PRIMARY KEY (`retailer_id`),
    CONSTRAINT `category_id_retailers` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`),
    CONSTRAINT `user_id_retailers` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
    );

-- Dumping structure for table shophub.retailer_loyalty
CREATE TABLE `retailer_loyalty` (
    `retailer_loyalty_id` int(11) NOT NULL AUTO_INCREMENT,
    `user_id` int(11) NOT NULL,
    `retailer_id` int(11) NOT NULL,
    `points` int(11) NOT NULL DEFAULT 0,
    `rewards_claimed` int(11) NOT NULL DEFAULT 0,
    PRIMARY KEY (`retailer_loyalty_id`),
    CONSTRAINT `retailer_id_retailer_loyalty` FOREIGN KEY (`retailer_id`) REFERENCES `retailers` (`retailer_id`),
    CONSTRAINT `user_id_retailer_loyalty` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
    );

-- Dumping structure for table shophub.social_media_platforms
CREATE TABLE `social_media_platforms` (
	`social_media_platform_id` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(50) NOT NULL DEFAULT '0',
	`logo_filename` varchar(50) NOT NULL DEFAULT '0',
	PRIMARY KEY (`social_media_platform_id`)
);

-- Dumping structure for table shophub.tags
CREATE TABLE `tags` (
	`tag_id` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(50) NOT NULL,
	PRIMARY KEY (`tag_id`)
);

-- Dumping structure for table shophub.retailer_social_media
CREATE TABLE `retailer_social_media` (
    `retailer_social_media_id` int(11) NOT NULL AUTO_INCREMENT,
    `retailer_id` int(11) NOT NULL DEFAULT 0,
    `social_media_platform_id` int(11) NOT NULL DEFAULT 0,
    `social_media_url` text NOT NULL,
    PRIMARY KEY (`retailer_social_media_id`),
    CONSTRAINT `retailer_id_retailer_social_media` FOREIGN KEY (`retailer_id`) REFERENCES `retailers` (`retailer_id`),
    CONSTRAINT `social_media_platform_id_retailer_social_media` FOREIGN KEY (`social_media_platform_id`) REFERENCES `social_media_platforms` (`social_media_platform_id`)
    );

-- Dumping structure for table shophub.retailer_tags
CREATE TABLE `retailer_tags` (
    `retailer_tag_id` int(11) NOT NULL AUTO_INCREMENT,
    `retailer_id` int(11) NOT NULL,
    `tag_id` int(11) DEFAULT NULL,
    PRIMARY KEY (`retailer_tag_id`),
    CONSTRAINT `retailer_id_retailer_tags` FOREIGN KEY (`retailer_id`) REFERENCES `retailers` (`retailer_id`),
    CONSTRAINT `tag_id_retailer_tags` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`tag_id`)
    );

CREATE TABLE `user_tags` (
    `user_tag_id` int(11) NOT NULL AUTO_INCREMENT,
    `user_id` int(11) NOT NULL,
    `tag_id` int(11) NOT NULL,
    PRIMARY KEY (`user_tag_id`),
    CONSTRAINT `user_id_user_tags` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
    CONSTRAINT `tag_id_user_tags` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`tag_id`)
    );


CREATE TABLE `events`(
	event_id INT auto_increment PRIMARY KEY,
	name VARCHAR(80)
    );

CREATE TABLE `logging`(
	logging_id INT auto_increment PRIMARY KEY,
	event_id INT,
	details VARCHAR(150),
    username VARCHAR(60),
    timestamp timestamp DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT `event_id_logging` FOREIGN KEY (`event_id`) REFERENCES `events` (`event_id`)
    );


SET FOREIGN_KEY_CHECKS=1;