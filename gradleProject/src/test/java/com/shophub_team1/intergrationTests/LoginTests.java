package com.shophub_team1.intergrationTests;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import javax.transaction.Transactional;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

//tests for registration controller
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@Transactional
public class LoginTests {

    @Autowired
    MockMvc mvc;

    @Test
    public void shouldRedirectToAdminHomeOnValidAdminLogin() throws Exception{
        //tests whether a valid admin login is redirected to the admin home page
        this.mvc.perform(multipart("/login")
                        .param("username","william@eddy.com")
                        .param("password", "12345")
                        .with(csrf())
                )
                .andDo(print())
                .andExpect(redirectedUrl("/admin/home"));

    }

    @Test
    public void shouldRedirectToDiscoverOnValidShopperLogin() throws Exception{
        //tests whether a valid shopper login is redirected to the discover page
        this.mvc.perform(multipart("/login")
                        .param("username","haroon@mohammed.com")
                        .param("password", "12345")
                        .with(csrf())
                )
                .andDo(print())
                .andExpect(redirectedUrl("/discover"));

    }

    @Test
    public void shouldRedirectToLoginErrorPageOnInvalidLogin() throws Exception{
        //tests whether an invalid login is redirected to the login error page
        this.mvc.perform(multipart("/login")
                        .param("username","user@notvalid.com")
                        .param("password", "notavalidpassword")
                        .with(csrf())
                )
                .andDo(print())
                .andExpect(redirectedUrl("/login?error"));

    }

}
