package com.shophub_team1.intergrationTests;

import com.shophub_team1.jpa.entity.RetailerEntity;
import com.shophub_team1.jpa.repository.RetailerRepository;
import com.shophub_team1.jpa.service.RetailerService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import static org.junit.jupiter.api.Assertions.assertEquals;

//tests for retailer profile controller
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureTestDatabase
public class RetailerProfileTests {

    @Autowired
    RetailerRepository rvp;

    @Autowired
    RetailerService rvs;

    // Check that the correct details are returned when searching by the retailer's name.
    @Test
    void companyDetailsReturnedWhenSearchByName() {

        String companyName = "Hugo Boss";

        RetailerEntity retailerDetails = rvp.getRetailerByName(companyName);

        assertEquals(2, retailerDetails.getRetailerId());
        assertEquals("hugoBoss.svg", retailerDetails.getLogo());
        assertEquals("Fashion and Clothing", retailerDetails.getCategory().getName());

    }

}
