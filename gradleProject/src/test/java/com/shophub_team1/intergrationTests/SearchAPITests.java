package com.shophub_team1.intergrationTests;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import javax.transaction.Transactional;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//tests for search API
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
@Transactional
@AutoConfigureTestDatabase
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@WithMockUser(username = "william@eddy.com", authorities={"Admin"})
public class SearchAPITests {

    @Autowired
    MockMvc mvc;

    @Test
    //test checks whether the search api is working (returns 200 status)
    public void searchApiShouldReturn200Status() throws Exception{
        this.mvc.perform(get("/api/search/all/fi"))
                .andDo(print())
                .andExpect(status().isOk())
        ;
    }

    @Test
    //test checks whether the search api returns a list of search results as json given a search parameter
    public void searchApiShouldReturnSearchResultJsonForGivenSearchString() throws Exception {
        this.mvc.perform(get("/api/search/all/as"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("[{'contextType':'retailer','name':'ASOS'},{'contextType':'category','name':'Fashion and Clothing'},{'contextType':'tag','name':'casual'},{'contextType':'tag','name':'fastfood'}]"))
        ;
    }

}
