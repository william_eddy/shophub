package com.shophub_team1.intergrationTests;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import javax.transaction.Transactional;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//tests for update retailer controller
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@Transactional
@WithMockUser(username = "william@eddy.com", authorities={"Admin"})

//test adapted from Haroon's full intergration test for adding retailers

public class UpdateRetailerTests {

    @Autowired
    MockMvc mvc;

    @Test
    public void updateRetailerWithFileUpload() throws Exception{

        //create PNG file for background image
        MockMultipartFile file
                = new MockMultipartFile(
                "backgroundImage",
                "hello.png",
                MediaType.TEXT_PLAIN_VALUE,
                "Hello, World!".getBytes()
        );

        //create SVG file for logo
        MockMultipartFile file2
                = new MockMultipartFile(
                "logo",
                "hello.svg",
                MediaType.TEXT_PLAIN_VALUE,
                "Hello, World!".getBytes()
        );

        //should add a new retailer to the database
        this.mvc.perform(multipart("/admin/retailer/edit")
                        .file(file)
                        .file(file2)
                        .param("retailerId","1")
                        .param("categoryId", "2")
                        .param("logoImagePath", "test.svc")
                        .param("backgroundImagePath", "test.png")
                        .param("name", "Levis Clothing")
                        .param("description","Added a description")
                        .param("maxPoints","10")
                        .param("categoryName", "Food and Drink")
                        .param("userId", "3")
                        .param("websiteUrl", "www.levis.com/testing")
                        .with(csrf())
                )
                .andDo(print())
                .andExpect(status().is3xxRedirection());

    }
}
