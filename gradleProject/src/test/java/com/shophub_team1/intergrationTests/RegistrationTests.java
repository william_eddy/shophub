package com.shophub_team1.intergrationTests;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//tests for registration controller
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@Transactional
@WithMockUser(username = "william@eddy.com", authorities={"Admin"})
public class RegistrationTests {

    @Autowired
    MockMvc mvc;

    @Test
    public void shouldRedirectOnSuccessfulCreationOfNewShopper() throws Exception{
        //tests whether a new user can be added using the POST request
        this.mvc.perform(multipart("/register/accountDetails")
                        .param("firstName","Test")
                        .param("lastName", "User")
                        .param("emailAddress","test@test.com")
                        .param("password","ThisIsATest1")
                        .with(csrf())
                )
                .andDo(print())
                .andExpect(status().is3xxRedirection());

    }

    @Test
    //tests whether user tags can be inserted using the POST request
    public void shouldReturnSuccessOnTagInsert() throws Exception
    {
        List<Long> tags = new ArrayList<>();
        tags.add(1L);
        tags.add(2L);
        tags.add(3L);

        mvc.perform( MockMvcRequestBuilders
                        .post("/register/tags")
                        .content(asJsonString(tags))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("success"));
    }

    //referenced from Masterspringboot
    //http://www.masterspringboot.com/testing/testing-spring-boot-applications-with-mockmvc/
    //accessed 12/12/2021 10:48

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    //end of reference
}
