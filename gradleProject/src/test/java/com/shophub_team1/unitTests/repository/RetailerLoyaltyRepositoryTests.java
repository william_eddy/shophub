package com.shophub_team1.unitTests.repository;

import com.shophub_team1.jpa.entity.RetailerLoyaltyEntity;
import com.shophub_team1.jpa.repository.RetailerLoyaltyRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;

//tests for retailer loyalty repository
@DataJpaTest
@ActiveProfiles("test")
public class RetailerLoyaltyRepositoryTests {

    @Autowired
    private RetailerLoyaltyRepository retailerLoyaltyRepository;

    //test checks whether the findLoyaltyCardsByUser method returns the loyalty cards for a user, given a userId
    @Test
    public void shouldReturnAllLoyaltyCardsGivenAUserId() {
        List<RetailerLoyaltyEntity> retailerLoyaltyEntitiesReturned = retailerLoyaltyRepository.findLoyaltyCardsByUser(1L);
        assertEquals(3,retailerLoyaltyEntitiesReturned.size(),"The findLoyaltyCardsByUser method did not return all of the loyalty cards for the userId given in the method parameters");
    }


}
