package com.shophub_team1.unitTests.repository;

import com.shophub_team1.jpa.entity.RetailerEntity;
import com.shophub_team1.jpa.repository.RetailerRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;

//tests for retailer repository
@DataJpaTest
@ActiveProfiles("test")
public class RetailerRepositoryTests {

    @Autowired
    private RetailerRepository retailerRepository;

    //test checks whether the getRetailersByCategory method returns retailers of a category, given a category name
    @Test
    public void shouldReturnAllRetailersOfGivenCategory() {
        List<RetailerEntity> retailerEntitiesReturned = retailerRepository.getRetailersByCategory("Fashion and Clothing");
        assertEquals(4,retailerEntitiesReturned.size(),"The getRetailersByCategory method did not return all of the retailers of the category given in the method parameters");
    }

    //test checks whether the getRetailersByName method returns a retailer, given a retailer name
    @Test
    public void shouldReturnRetailerOfGivenName() {
        RetailerEntity retailerEntityReturned = retailerRepository.getRetailerByName("Hugo Boss");
        assertEquals(2L, retailerEntityReturned.getRetailerId(),"The getRetailersByName method did not return the retailer of the name given in the method parameters");
    }

    //test checks whether the searchRetailerNames method returns a list of retailers with names similar to the search string
    @Test
    public void shouldReturnRetailerNamesSimilarToSearchString() {
        List<String> retailerNamesReturned = retailerRepository.searchRetailerNames("vi");
        assertEquals(2, retailerNamesReturned.size(),"The searchRetailerNames method did not return all of the retailer names similar to the search string given in the method parameters");
    }

    //test checks whether the findByTags_name method returns a list of retailers with a given tag associated to them
    @Test
    public void shouldReturnAllRetailerWithAssociatedTag() {
        List<RetailerEntity> retailersReturned = retailerRepository.findByTags_name("independent");
        assertEquals(3, retailersReturned.size(),"The findByTags_name method did not return all of the retailers associated with the tag given in the method parameters");
    }

}
