package com.shophub_team1.unitTests.repository;

import com.shophub_team1.jpa.repository.UserTagRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import static org.junit.jupiter.api.Assertions.assertEquals;

//tests for user tag repository
@DataJpaTest
@ActiveProfiles("test")
public class UserTagRepositoryTests {

    @Autowired
    private UserTagRepository userTagRepository;

    //test checks whether the countUserTagsByUser method returns the number of tags for a user, given the userId
    @Test
    public void shouldReturnCountOfTagsForUser() {
        Long numberOfTagsReturned = userTagRepository.countUserTagsByUser(1L);
        assertEquals(3, numberOfTagsReturned.intValue(),"The searchTagNames method did not return all of the tag names similar to the search string given in the method parameters");
    }

}
