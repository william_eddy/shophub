package com.shophub_team1.unitTests.repository;

import com.shophub_team1.jpa.entity.CategoryEntity;
import com.shophub_team1.jpa.repository.CategoryRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;

//tests for category repository
@DataJpaTest
@ActiveProfiles("test")
public class CategoryRepositoryTests {

    @Autowired
    private CategoryRepository categoryRepository;

    //test checks whether the getCategoryByName method returns the record for a category, given a name
    @Test
    public void shouldReturnACategoryRecordGivenACategoryName() {
        CategoryEntity categoryExpected = new CategoryEntity(1L, "Food and Drink");
        CategoryEntity categoryReturned = categoryRepository.getCategoryByName("Food and Drink");
        assertTrue(categoryReturned.equals(categoryExpected),"The getCategoryByName method did not return the category record for the name given in the method parameters");
    }

    //test checks whether the getCategoryByIdName method returns the categoryId for a category, given a name
    @Test
    public void shouldReturnACategoryIdGivenACategoryName() {
        Optional<Long> categoryIdReturned = categoryRepository.getCategoryIDByName("Food and Drink");
        assertEquals(1L,categoryIdReturned.get(),"The getCategoryIdByName method did not return the categoryId for the name given in the method parameters");
    }

    //test checks whether the searchCategoryNames method returns category names similar to a given search parameter
    @Test
    public void shouldReturnCategoryNamesSimilarToSearchParameter() {
        List<String> categoryNamesReturned = categoryRepository.searchCategoryNames("Fa");
        List<String> expectedResult = new ArrayList<>();
        expectedResult.add("Fashion and Clothing");
        assertEquals(expectedResult,categoryNamesReturned,"The searchCategoryNames method did not return the category names similar to the search string given in the method parameters");
    }


}
