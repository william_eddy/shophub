package com.shophub_team1.unitTests.repository;

import com.shophub_team1.jpa.entity.LoggingEntity;
import com.shophub_team1.jpa.repository.LoggingRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import static org.junit.jupiter.api.Assertions.assertEquals;

//tests for logging repository
@DataJpaTest
@ActiveProfiles("test")
public class LoggingTests {

    @Autowired
    private LoggingRepository loggingRepository;

    //test checks whether the save method adds a new log to the database
    @Test
    public void shouldSaveNewLogIntoTestDatabase() {
        Integer numberOfRecordsBefore = loggingRepository.findAll().size();
        LoggingEntity newLog = new LoggingEntity(1L,"details","test_user");
        loggingRepository.save(newLog);
        Integer numberOfRecordsAfter = loggingRepository.findAll().size();
        assertEquals((numberOfRecordsBefore + 1),numberOfRecordsAfter,"The getCategoryByName method did not return the category record for the name given in the method parameters");
    }
}
