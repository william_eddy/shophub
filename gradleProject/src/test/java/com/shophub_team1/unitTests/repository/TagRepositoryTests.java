package com.shophub_team1.unitTests.repository;

import com.shophub_team1.jpa.repository.TagRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;

//tests for tag repository
@DataJpaTest
@ActiveProfiles("test")
public class TagRepositoryTests {

    @Autowired
    private TagRepository tagRepository;

    //test checks whether the searchTagNames method returns a list of tag with names similar to the search string
    @Test
    public void shouldReturnTagNamesSimilarToSearchString() {
        List<String> tagNamesReturned = tagRepository.searchTagNames("fo");
        assertEquals(3, tagNamesReturned.size(),"The searchTagNames method did not return all of the tag names similar to the search string given in the method parameters");
    }

}
