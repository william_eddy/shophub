package com.shophub_team1.unitTests.service;

import com.shophub_team1.jpa.repository.CategoryRepository;
import com.shophub_team1.jpa.service.AdminAddRetailerService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;

//tests for admin add retailer service
@SpringBootTest
@ActiveProfiles("test")
public class AdminAddRetailerServiceTests {

    @MockBean
    private CategoryRepository categoryRepositoryMock;

    @Autowired
    AdminAddRetailerService adminAddRetailerService;

    //test checks whether the doesCategoryExist method returns true for a category that exists, given a category name
    @Test
    public void shouldReturnTrueForACategoryThatExistsGivenACategoryId() {
        doReturn(Optional.of(1L)).when(categoryRepositoryMock).getCategoryIDByName("Food and Drink");
        Boolean doesCategoryExist = adminAddRetailerService.doesCategoryExist("Food and Drink");
        assertTrue(doesCategoryExist,"The doesCategoryExist method returned false for a category that exists");
    }

    //test checks whether the doesCategoryExist method returns false for a category that does not exist, given a category name
    @Test
    public void shouldReturnFalseForACategoryThatDoesNotExistGivenACategoryId() {
        doReturn(Optional.of(1L)).when(categoryRepositoryMock).getCategoryIDByName("Food and Drink");
        Boolean doesCategoryExist = adminAddRetailerService.doesCategoryExist("Home Store");
        assertFalse(doesCategoryExist,"The doesCategoryExist method returned true for a category that does not exist");
    }

}
