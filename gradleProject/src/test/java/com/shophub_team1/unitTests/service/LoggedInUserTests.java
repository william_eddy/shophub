package com.shophub_team1.unitTests.service;

import com.shophub_team1.jpa.entity.RoleEntity;
import com.shophub_team1.jpa.entity.UserEntity;
import com.shophub_team1.jpa.repository.UserRepository;
import com.shophub_team1.jpa.service.LoggedInUserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import javax.transaction.Transactional;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.doReturn;

//tests for logged in user service
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureTestDatabase
@Transactional
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class LoggedInUserTests {

    @MockBean
    private UserRepository userRepositoryMock;

    @Autowired
    LoggedInUserService loggedInUserService;

    UserEntity expectedMockResult = new UserEntity(1L,"William","Eddy","william@eddy.com","password","picture.png", new RoleEntity(1L,"Admin"));

    //test checks whether the getUserId method returns the userId for a user that is logged in
    @Test
    @WithMockUser(username = "william@eddy.com", authorities={"Admin"})
    public void shouldReturnTheUserIdOfTheUserLoggedIn() {
        doReturn(Optional.of(expectedMockResult)).when(userRepositoryMock).findByEmailAddress("william@eddy.com");
        Long actualUserId = loggedInUserService.getUserId();
        assertEquals(1L,actualUserId,"The getUserId method did not return the userId for the mocked user declared in the test class");
    }

    //test checks whether the getUserId method returns null when no user is logged in
    @Test
    public void shouldReturnNullForGetUserIdWhenNoUserLoggedIn() {
        assertNull(loggedInUserService.getUserId(),"The getUserId method did not return null when no user was logged in");
    }

    //test checks whether the getFirstName method returns the firstName for a user that is logged in
    @Test
    @WithMockUser(username = "william@eddy.com", authorities={"Admin"})
    public void shouldReturnTheFirstNameOfTheUserLoggedIn() {
        doReturn(Optional.of(expectedMockResult)).when(userRepositoryMock).findByEmailAddress("william@eddy.com");
        doReturn(Optional.of(expectedMockResult)).when(userRepositoryMock).findById(1L);
        String actualFirstName = loggedInUserService.getFirstName();
        assertEquals("William",actualFirstName,"The getFirstName method did not return the firstName for the mocked user declared in the test class");
    }

    //test checks whether the getFirstName method returns null when no user is logged in
    @Test
    public void shouldReturnNullForGetFirstNameWhenNoUserLoggedIn() {
        assertNull(loggedInUserService.getFirstName(),"The getFirstName method did not return null when no user was logged in");
    }

    //test checks whether the getLastName method returns the lastName for a user that is logged in
    @Test
    @WithMockUser(username = "william@eddy.com", authorities={"Admin"})
    public void shouldReturnTheLastNameOfTheUserLoggedIn() {
        doReturn(Optional.of(expectedMockResult)).when(userRepositoryMock).findByEmailAddress("william@eddy.com");
        doReturn(Optional.of(expectedMockResult)).when(userRepositoryMock).findById(1L);
        String actualLastName = loggedInUserService.getLastName();
        assertEquals("Eddy",actualLastName,"The getLastName method did not return the lastName for the mocked user declared in the test class");
    }

    //test checks whether the getLastName method returns null when no user is logged in
    @Test
    public void shouldReturnNullForGetLastNameWhenNoUserLoggedIn() {
        assertNull(loggedInUserService.getFirstName(),"The getLastName method did not return null when no user was logged in");
    }

    //test checks whether the getProfilePicture method returns the profilePicture for a user that is logged in
    @Test
    @WithMockUser(username = "william@eddy.com", authorities={"Admin"})
    public void shouldReturnTheProfilePictureOfTheUserLoggedIn() {
        doReturn(Optional.of(expectedMockResult)).when(userRepositoryMock).findByEmailAddress("william@eddy.com");
        doReturn(Optional.of(expectedMockResult)).when(userRepositoryMock).findById(1L);
        String actualProfilePic = loggedInUserService.getProfilePicture();
        assertEquals("picture.png",actualProfilePic,"The getProfilePicture method did not return the profilePicture for the mocked user declared in the test class");
    }

    //test checks whether the getProfilePicture method returns null when no user is logged in
    @Test
    public void shouldReturnNullForGetProfilePictureWhenNoUserLoggedIn() {
        assertNull(loggedInUserService.getProfilePicture(),"The getProfilePicture method did not return null when no user was logged in");
    }


    //test checks whether the getRole method returns the role for a user that is logged in
    @Test
    @WithMockUser(username = "william@eddy.com", authorities={"Admin"})
    public void shouldReturnTheRoleOfTheUserLoggedIn() {
        doReturn(Optional.of(expectedMockResult)).when(userRepositoryMock).findByEmailAddress("william@eddy.com");
        doReturn(Optional.of(expectedMockResult)).when(userRepositoryMock).findById(1L);
        String actualRole = loggedInUserService.getRole();
        assertEquals("Admin",actualRole,"The getRole method did not return the role for the mocked user declared in the test class");
    }

    //test checks whether the getRole method returns null when no user is logged in
    @Test
    public void shouldReturnNullForGetRoleWhenNoUserLoggedIn() {
        assertNull(loggedInUserService.getRole(),"The getRole method did not return null when no user was logged in");
    }


}
