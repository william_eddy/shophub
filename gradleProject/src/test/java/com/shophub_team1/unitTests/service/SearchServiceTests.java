package com.shophub_team1.unitTests.service;

import com.shophub_team1.jpa.miscObjects.SearchResult;
import com.shophub_team1.jpa.repository.CategoryRepository;
import com.shophub_team1.jpa.repository.RetailerRepository;
import com.shophub_team1.jpa.repository.TagRepository;
import com.shophub_team1.jpa.service.SearchService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;

//tests for search service
@SpringBootTest
public class SearchServiceTests {

    @MockBean
    private RetailerRepository retailerRepository;

    @MockBean
    private CategoryRepository categoryRepository;

    @MockBean
    private TagRepository tagRepository;

    @Autowired
    SearchService searchService;

    //test checks whether the getAllSearchResults method returns retailers, tags and categories similar to a given search string
    @Test
    public void shouldReturnRetailersTagsAndCategoriesSimilarToGivenSearchString() {

        String searchParameter = "as";
        List<String> retailersMockResult = new ArrayList<>();
        retailersMockResult.add("ASOS");

        List<String> tagsMockResult = new ArrayList<>();
        tagsMockResult.add("casual");
        tagsMockResult.add("fastfood");

        List<String> categoriesMockResult = new ArrayList<>();
        categoriesMockResult.add("Fashion and Clothing");

        doReturn(retailersMockResult).when(retailerRepository).searchRetailerNames(searchParameter);
        doReturn(tagsMockResult).when(tagRepository).searchTagNames(searchParameter);
        doReturn(categoriesMockResult).when(categoryRepository).searchCategoryNames(searchParameter);

        List<SearchResult> searchResults = searchService.getAllSearchResults(searchParameter);

        assertEquals(4,searchResults.size(),"The getAllSearchResults method did not return the correct number of search results for the parameters given in the method");
    }


}
