How to create key files and passwords -

OPENSSL commands -

ALG 1 - openssl rand -hex 32 > <path to file> : 256 bit encryption key generate
ALG 2 - openssl rand -hex 128 > <path to file.key> : Generates a encryption password for the keys that can be 256 characters long
ALG 3 - openssl enc -aes-256-cbc -md sha256 -pass <FILE:path to password file> -in <path to the key file> -out <path to the output.enc>

Loosely follow this tutorial here:

https://mariadb.com/kb/en/file-key-management-encryption-plugin/

Or, alternatives:

https://andysh.uk/2018/07/mariadb-encryption-database-gdpr/
https://www.labsrc.com/please-encrypt-your-databases-mariadb/

Tutorial:

Open git in the MariaDB 10.6\data file. Proceed to use Algorithm 1 with openSSL. Point towards appropriate path, generate as many keys
as neccesary. Once complete, open file and add 1; to the first key, 2; to the second and so on and so forth. Realistically, however, only
one key is required to be generated at this time.

Use algorithm 2 to create a password in the same directory. This is to be stored safely somewhere else at a later time. 
Use algorithm 3 to generate the encrypted key file for MariaDB to use. Note in the provided files I used SHA-1 instead of SHA-256. 

Config the my.ini file with the format provided. Some important ideas here are:

plugin_load_add=file_key_management.dll
file_key_management = ON
Vairables here activate the mariadb plugin.

loose_file_key_management_filename = C:/Program Files/MariaDB 10.6/data/mysql/encryption/keyfile.enc
loose_file_key_management_filekey = FILE:C:/Program Files/MariaDB 10.6/data/mysql/encryption/keyfile.key
Variables here point to the key directories. loose is provided to prevent errors stopping mariadb running
should one arise. Ensure to describe the filekey as a file with FILE:

innodb_encrypt_tables = ON
innodb_encrypt_log = ON
encrypt_tmp_disk_tables = ON
innodb_encryption_threads = 8
Variables here allow innodb to encrypt tables. All tables should be innodb anyway. Encryption threads
manages the amount of threads working on encryption at a time - if encryption is slow and the device
can handle more, up the threads.

To enact these changes, an initial reboot of MariaDB will need to be triggered. WINDOWS + R key should
activate run, from there find the MariaDB service, not the MySQL service, and force restart it. If it
fails to start, check the log generated in the data file as an error has occured with config. 

Upon restart, table encryption will begin. This can be observed with the following SQL:

SELECT COUNT(*) AS "Number of Encrypted Tablespaces"
FROM information_schema.INNODB_TABLESPACES_ENCRYPTION
WHERE ENCRYPTION_SCHEME != 0
   OR ROTATING_OR_FLUSHING != 0; 

Check INNODB_TABLESPACES_ENCRYPTION table for the tables that have been encrypted. 

To physically check if a table is encrypted, use SET GLOBAL innodb_encrypt_tables = OFF, then go to data and select one of your databases.
Open one of the table files with the .ibd extension with a sufficient text editor (I used notepad++), and observe that some useable data can be extracted.
Proceed to SET GLOBAL innodb_encrypt_tables = ON, then observe the same file. The table has been encrypted. 

An example of the above has been provided with an example key set using SHA-1 - I will replicate this when the time comes to it with SHA-256. 

