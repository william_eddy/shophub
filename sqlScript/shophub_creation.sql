-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.5.9-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for shophub
DROP SCHEMA IF EXISTS `shophub`;
CREATE DATABASE IF NOT EXISTS `shophub` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `shophub`;

-- Dumping structure for table shophub.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`category_id`) USING BTREE
);

-- Dumping structure for table shophub.retailers
CREATE TABLE IF NOT EXISTS `retailers` (
  `retailer_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL UNIQUE,
  `description` MEDIUMTEXT DEFAULT NULL,
  `logo` varchar(50) NOT NULL,
  `background_image` varchar(50) NOT NULL,
  `max_points` int(11) DEFAULT 0,
  `category_id` int(11) DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `website_url` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`retailer_id`) USING BTREE,
  KEY `category_id_retailers` (`category_id`),
  KEY `user_id_retailers` (`user_id`),
  CONSTRAINT `category_id_retailers` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`),
  CONSTRAINT `user_id_retailers` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
);

-- Dumping structure for table shophub.retailer_loyalty
CREATE TABLE IF NOT EXISTS `retailer_loyalty` (
  `retailer_loyalty_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `retailer_id` int(11) NOT NULL,
  `points` int(11) NOT NULL DEFAULT 0,
  `points_redeemed` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`retailer_loyalty_id`) USING BTREE,
  KEY `user_id_retailer_loyalty` (`user_id`),
  KEY `retailer_id_retailer_loyalty` (`retailer_id`),
  CONSTRAINT `retailer_id_retailer_loyalty` FOREIGN KEY (`retailer_id`) REFERENCES `retailers` (`retailer_id`),
  CONSTRAINT `user_id_retailer_loyalty` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
);

-- Dumping structure for table shophub.retailer_social_media
CREATE TABLE IF NOT EXISTS `retailer_social_media` (
  `retailer_social_media_id` int(11) NOT NULL AUTO_INCREMENT,
  `retailer_id` int(11) NOT NULL DEFAULT 0,
  `social_media_platform_id` int(11) NOT NULL DEFAULT 0,
  `social_media_url` text NOT NULL,
  PRIMARY KEY (`retailer_social_media_id`) USING BTREE,
  KEY `retailer_id_retailer_social_media` (`retailer_id`),
  KEY `social_media_platform_id_retailer_social_media` (`social_media_platform_id`),
  CONSTRAINT `retailer_id_retailer_social_media` FOREIGN KEY (`retailer_id`) REFERENCES `retailers` (`retailer_id`),
  CONSTRAINT `social_media_platform_id_retailer_social_media` FOREIGN KEY (`social_media_platform_id`) REFERENCES `social_media_platforms` (`social_media_platform_id`)
);

-- Dumping structure for table shophub.retailer_tags
CREATE TABLE IF NOT EXISTS `retailer_tags` (
  `retailer_tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `retailer_id` int(11) NOT NULL,
  `tag_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`retailer_tag_id`) USING BTREE,
  KEY `retailer_id_retailer_tags` (`retailer_id`),
  KEY `tag_id_retailer_tags` (`tag_id`),
  CONSTRAINT `retailer_id_retailer_tags` FOREIGN KEY (`retailer_id`) REFERENCES `retailers` (`retailer_id`),
  CONSTRAINT `tag_id_retailer_tags` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`tag_id`)
);

CREATE TABLE IF NOT EXISTS `user_tags` (
  `user_tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`user_tag_id`) USING BTREE,
  KEY `user_id_user_tags` (`user_id`),
  KEY `tag_id_user_tags` (`tag_id`),
  CONSTRAINT `user_id_user_tags` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `tag_id_user_tags` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`tag_id`)
);

-- Dumping structure for table shophub.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`role_id`) USING BTREE
);

-- Dumping structure for table shophub.social_media_platforms
CREATE TABLE IF NOT EXISTS `social_media_platforms` (
  `social_media_platform_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '0',
  `icon_class_name` varchar(50) NOT NULL DEFAULT '0',
  `hex_code` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`social_media_platform_id`) USING BTREE
);

-- Dumping structure for table shophub.tags
CREATE TABLE IF NOT EXISTS `tags` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`tag_id`) USING BTREE
);

-- Dumping structure for table shophub.users
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL DEFAULT '0',
  `last_name` varchar(50) NOT NULL DEFAULT '0',
  `email_address` varchar(50) UNIQUE NOT NULL DEFAULT '0',
  `password` varchar(50) NOT NULL DEFAULT '0',
  `profile_picture` varchar(50) DEFAULT 'defaultProfile.jpg',
  `role_id` int(11) NOT NULL DEFAULT 3,
  PRIMARY KEY (`user_id`) USING BTREE,
  KEY `role_id_users` (`role_id`),
  CONSTRAINT `role_id_users` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`)
);


CREATE TABLE IF NOT EXISTS `events`(
	event_id INT auto_increment PRIMARY KEY,
    name VARCHAR(80)
);

CREATE TABLE IF NOT EXISTS `logging`(
	logging_id INT auto_increment PRIMARY KEY,
    event_id INT,
    details VARCHAR(150),
    username VARCHAR(60),
    timestamp timestamp DEFAULT CURRENT_TIMESTAMP,
    KEY `event_id_logging` (`event_id`),
	CONSTRAINT `event_id_logging` FOREIGN KEY (`event_id`) REFERENCES `events` (`event_id`)
);

CREATE VIEW `loggingView` AS(
	SELECT l.logging_id, e.name as event, l.details, l.username, l.timestamp
    FROM logging l
    JOIN events e
    ON e.event_id = l.event_id
);

DELIMITER //
CREATE PROCEDURE UPDATE_USER(in user_id_in long, in first_name_in varchar(50), in last_name_in varchar(50), in email_address_in varchar(50), in password_in varchar(50), in profile_picture_in varchar(50))
BEGIN
	UPDATE shophub.users
    set 
		first_name = first_name_in,
        last_name = last_name_in,
        email_address = email_address_in,
        password = password_in,
        profile_picture = profile_picture_in
	where user_id = user_id_in;
END//
DELIMITER ;

-- Adding Test Data To Use
SET FOREIGN_KEY_CHECKS=0;

USE `shophub`;


-- Category Inserts

INSERT INTO `categories` (`category_id`, `name`) VALUES
	(1, 'Food and Drink'),
	(2, 'Fashion and Clothing'),
	(3, 'Distillery'),
    (4, 'Home Store');
    
--


-- Retailer Inserts

INSERT INTO `retailers` (`retailer_id`, `name`, `description`, `logo`, `background_image`, `max_points`, `category_id`, `user_id`, `website_url`) VALUES
	(1,'Levis', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum.', 'levi.svg', 'leviBg.png', 5, 2, 3, 'www.levis.com'),
	(2,'Hugo Boss', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum.', 'hugoBoss.svg', 'hugoBossBg.png', 5, 2, 3, 'www.hugoboss.com/uk/home'),
	(3,'ASOS', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum.', 'asos.svg', 'asosBg.png', 5, 2, 3, 'www.asos.co.uk'),
	(4,'Five Guys', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum.', 'fiveGuys.svg', 'fiveGuysBg.png', 5, 1, 3, 'www.fiveguys.co.uk'),
	(5,'Benettis Coffee', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum.', 'benettis.svg', 'benettisBg.png', 5, 1, 3, 'www.instagram.com/benettiscoffee/?hl=en'),
	(6,'Warners', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum.', 'warners.svg', 'warnersBg.png', 5, 1, 3, 'www.warnersdistillery.com/'),
	(7,'Ye Old Sweet Shop', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum.', 'yeOldSweetShop.svg', 'yeOldSweetShopBg.png', 5, 1, 3, 'www.oldestsweetshop.co.uk/'),
	(8,'Mary and Me', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum.', 'maryAndMe.svg', 'maryAndMeBg.png', 5, 4, 3, 'www.facebook.com/meandmary/'),
	(9,'Vintage Co', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum.', 'vintageCo.svg', 'vintageCoBg.png', 5, 2, 3, 'www.vintage.co.uk/');
    
-- 


INSERT INTO `retailer_loyalty` (`retailer_loyalty_id`, `user_id`, `retailer_id`, `points`, `points_redeemed`) VALUES
	(1, 3, 6, 2, 0);
    
INSERT INTO `retailer_loyalty` (`retailer_loyalty_id`, `user_id`, `retailer_id`, `points`, `points_redeemed`) VALUES
    (2, 3, 5, 0, 0);
    
INSERT INTO `retailer_loyalty` (`retailer_loyalty_id`, `user_id`, `retailer_id`, `points`, `points_redeemed`) VALUES
    (3, 3, 3, 2, 0);
    
INSERT INTO `retailer_loyalty` (`retailer_loyalty_id`, `user_id`, `retailer_id`, `points`, `points_redeemed`) VALUES
    (4, 3, 1, 0, 0);
    
INSERT INTO `retailer_loyalty` (`retailer_loyalty_id`, `user_id`, `retailer_id`, `points`, `points_redeemed`) VALUES
    (5, 1, 1, 0, 0);
    
INSERT INTO `retailer_loyalty` (`retailer_loyalty_id`, `user_id`, `retailer_id`, `points`, `points_redeemed`) VALUES
    (6, 1, 2, 0, 0);
    
INSERT INTO `retailer_loyalty` (`retailer_loyalty_id`, `user_id`, `retailer_id`, `points`, `points_redeemed`) VALUES
    (7, 1, 3, 0, 0);



INSERT INTO `retailer_social_media` (`retailer_social_media_id`, `retailer_id`, `social_media_platform_id`, `social_media_url`) VALUES
	(1, 2, 1, 'www.facebook.com/hugoboss'),
    (2, 2, 2, 'www.twitter.com/hugoboss'),
    (3, 2, 3, 'www.instagram.com/hugoboss');


INSERT INTO `retailer_tags` (`retailer_id`, `tag_id`) VALUES
	(1, 1),
    (1, 2),
    (1, 3),
    (1, 4),
    (1, 5),
    
    (2, 6),
    (2, 7),
    (2, 8),
    (2, 9),
    (2, 10),
    
    (3, 11),
    (3, 12),
    (3, 1),
    (3, 13),
    (3, 14),
    (3, 15),
    
	(4, 16),
    (4, 17),
    (4, 18),
    (4, 19),
    (4, 20),
    (4, 21),
    (4, 22),
    
    (5, 23),
    (5, 22),
    (5, 24),
    (5, 25),
    
    (6, 26),
    (6, 27),
    (6, 28),
    (6, 29),
    (6, 30),
    (6, 31),
    
	(7, 32),
    (7, 16),
    (7, 24),
    (7, 22),
    (7, 35),
    
    (8, 33),
    (8, 34),
    (8, 35),
    (8, 36),
    (8, 24),
    
    (9, 25),
    (9, 1);

INSERT INTO `user_tags` (`user_id`, `tag_id`) VALUES
	(1, 1),
    (1, 2),
    (1, 3),
    (3, 1),
    (3, 2),
    (3, 3);
   

INSERT INTO `roles` (`role_id`, `name`) VALUES
	(1, 'Admin'),
	(2, 'Retailer'),
	(3, 'Shopper');
    

INSERT INTO `social_media_platforms` (`social_media_platform_id`, `name`, `icon_class_name`, `hex_code`) VALUES
	(1, 'Facebook', 'fa fa-facebook-f', '#3b5998'),
	(2, 'Twitter', 'fa fa-twitter', '#00acee'),
	(3, 'Instagram', 'fa fa-instagram', '#E3454C');


INSERT INTO `tags` (`tag_id`, `name`) VALUES
	(1, 'clothing'),
    (2, 'jeans'),
    (3, 'menswear'),
    (4, 'hats'),
    (5, 'jumpers'),
    (6, 'formalwear'),
    (7, 'suits'),
    (8, 'shoes'),
    (9, 'professional'),
    (10, 'workwear'),
    (11,'trendy'),
    (12, 'boots'),
    (13, 'outerwear'),
    (14, 'style'),
    (15, 'casual'),
    (16, 'food'),
    (17, 'drink'),
    (18, 'burgers'),
    (19, 'fastfood'),
    (20, 'fries'),
    (21, 'milkshakes'),
    (22, 'vegan'),
    (23, 'coffee'),
    (24, 'independent'),
	(25, 'sustainable'),
    (26, 'gin'),
    (27, 'distillery'),
    (28, 'presents'),
    (29, 'alcohol'),
    (30, 'drinks'),
    (31, 'tonic'),
    (32, 'sweets'),
    (33, 'design'),
    (34, 'decor'),
    (35, 'modern'),
    (36, 'interior');


INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `email_address`, `password`, `profile_picture`, `role_id`) VALUES
	(1, 'William', 'Eddy', 'william@eddy.com', '12345', 'will.png', 1),
    (2, 'Jared', 'Brokenshire', 'jared@brokenshire.com', '12345', 'jared.png', 2),
    (3, 'Haroon', 'Mohammed', 'haroon@mohammed.com', '12345', 'haroon.png', 3);

INSERT INTO `events`(`event_id`,`name`) VALUES
 (1, "Other"),
 (2, "Successful login"),
 (3, "Unsuccessful login"),
 (4, "New retailer added"),
 (5, "Retailer edited"),
 (6, "Account details changed");

SET FOREIGN_KEY_CHECKS=1;


/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;



DELIMITER //
CREATE PROCEDURE UPDATE_RETAILER_WITHOUT_NAME_CHANGE(in name_in varchar(50), in description_in text, in logo_in varchar(50), in background_image_in varchar(50),
                                           in max_points_in int, in category_id_in long, in user_id_in long, website_url_in varchar(50))
BEGIN
UPDATE shophub.retailers
SET
    description = description_in,
    logo = logo_in,
    background_image = background_image_in,
    max_points = max_points_in,
    category_id = category_id_in,
    user_id = user_id_in,
    website_url = website_url_in
where name = name_in;
END//
DELIMITER ;

DELIMITER //
CREATE PROCEDURE UPDATE_RETAILER_WITH_NAME_CHANGE(in retailer_id_in long, in name_in varchar(50), in description_in text, in logo_in varchar(50), in background_image_in varchar(50),
                                              in max_points_in int, in category_id_in long, in user_id_in long, website_url_in varchar(50))
BEGIN
UPDATE shophub.retailers
SET
    name = name_in,
    description = description_in,
    logo = logo_in,
    background_image = background_image_in,
    max_points = max_points_in,
    category_id = category_id_in,
    user_id = user_id_in,
    website_url = website_url_in
where retailer_id = retailer_id_in;
END//
DELIMITER ;

SET FOREIGN_KEY_CHECKS=1;

-- Creating the user for shophub
CREATE USER IF NOT EXISTS 'ShopHub'@'localhost' IDENTIFIED BY 'dvhjuyt567yhb-%mkuytfdfn';
GRANT SELECT, UPDATE, INSERT ON shophub.users TO 'ShopHub'@'localhost';
GRANT SELECT, UPDATE, INSERT ON shophub.categories TO 'ShopHub'@'localhost';
GRANT SELECT, UPDATE, INSERT ON shophub.retailer_loyalty TO 'ShopHub'@'localhost';
GRANT SELECT, INSERT ON shophub.retailer_social_media TO 'ShopHub'@'localhost';
GRANT SELECT, UPDATE, INSERT ON shophub.retailer_tags TO 'ShopHub'@'localhost';
GRANT SELECT, UPDATE, INSERT ON shophub.user_tags TO 'ShopHub'@'localhost';
GRANT SELECT, UPDATE, INSERT ON shophub.retailers TO 'ShopHub'@'localhost';
GRANT SELECT, UPDATE ON shophub.roles TO 'ShopHub'@'localhost';
GRANT SELECT, INSERT ON shophub.social_media_platforms TO 'ShopHub'@'localhost';
GRANT SELECT, UPDATE, INSERT ON shophub.tags TO 'ShopHub'@'localhost';
GRANT INSERT, SELECT ON shophub.logging TO 'ShopHub'@'localhost';
GRANT INSERT, SELECT ON shophub.events TO 'ShopHub'@'localhost';
GRANT EXECUTE ON PROCEDURE UPDATE_RETAILER_WITH_NAME_CHANGE TO 'ShopHub'@'localhost';
GRANT EXECUTE ON PROCEDURE UPDATE_RETAILER_WITHOUT_NAME_CHANGE TO 'ShopHub'@'localhost';